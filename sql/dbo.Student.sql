﻿CREATE TABLE [dbo].[Student]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWID(), 
    [Name] NVARCHAR(50) NOT NULL DEFAULT '', 
    [StudentCardNumber] NVARCHAR(50) NOT NULL DEFAULT '', 
    [Password] NVARCHAR(100) NOT NULL DEFAULT ''
)

GO

CREATE INDEX [PrimaryColumnIndex] ON [dbo].[Students] ([Id])
