﻿namespace Diplom
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.page_chooseLab = new System.Windows.Forms.TabPage();
			this.b_startTesting = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.comB_chooseLabWork = new System.Windows.Forms.ComboBox();
			this.comB_chooseDiscipline = new System.Windows.Forms.ComboBox();
			this.page_testing = new System.Windows.Forms.TabPage();
			this.b_nextQuestion = new System.Windows.Forms.Button();
			this.rb_var4 = new System.Windows.Forms.RadioButton();
			this.rb_var3 = new System.Windows.Forms.RadioButton();
			this.rb_var2 = new System.Windows.Forms.RadioButton();
			this.rb_var1 = new System.Windows.Forms.RadioButton();
			this.cb_var4 = new System.Windows.Forms.CheckBox();
			this.cb_var3 = new System.Windows.Forms.CheckBox();
			this.cb_var2 = new System.Windows.Forms.CheckBox();
			this.cb_var1 = new System.Windows.Forms.CheckBox();
			this.tb_questionText = new System.Windows.Forms.TextBox();
			this.tabControl1.SuspendLayout();
			this.page_chooseLab.SuspendLayout();
			this.page_testing.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.page_chooseLab);
			this.tabControl1.Controls.Add(this.page_testing);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(666, 447);
			this.tabControl1.TabIndex = 0;
			// 
			// page_chooseLab
			// 
			this.page_chooseLab.Controls.Add(this.b_startTesting);
			this.page_chooseLab.Controls.Add(this.label2);
			this.page_chooseLab.Controls.Add(this.label1);
			this.page_chooseLab.Controls.Add(this.comB_chooseLabWork);
			this.page_chooseLab.Controls.Add(this.comB_chooseDiscipline);
			this.page_chooseLab.Location = new System.Drawing.Point(4, 22);
			this.page_chooseLab.Name = "page_chooseLab";
			this.page_chooseLab.Padding = new System.Windows.Forms.Padding(3);
			this.page_chooseLab.Size = new System.Drawing.Size(658, 421);
			this.page_chooseLab.TabIndex = 0;
			this.page_chooseLab.Text = "Вибір тесту";
			this.page_chooseLab.UseVisualStyleBackColor = true;
			this.page_chooseLab.Enter += new System.EventHandler(this.page_chooseLab_Enter);
			// 
			// b_startTesting
			// 
			this.b_startTesting.Location = new System.Drawing.Point(152, 135);
			this.b_startTesting.Name = "b_startTesting";
			this.b_startTesting.Size = new System.Drawing.Size(106, 43);
			this.b_startTesting.TabIndex = 4;
			this.b_startTesting.Text = "Розпочати тестування";
			this.b_startTesting.UseVisualStyleBackColor = true;
			this.b_startTesting.Click += new System.EventHandler(this.b_startTesting_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(62, 83);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Лабораторна робота";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(62, 47);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Предмет";
			// 
			// comB_chooseLabWork
			// 
			this.comB_chooseLabWork.FormattingEnabled = true;
			this.comB_chooseLabWork.Location = new System.Drawing.Point(197, 80);
			this.comB_chooseLabWork.Name = "comB_chooseLabWork";
			this.comB_chooseLabWork.Size = new System.Drawing.Size(207, 21);
			this.comB_chooseLabWork.TabIndex = 1;
			// 
			// comB_chooseDiscipline
			// 
			this.comB_chooseDiscipline.FormattingEnabled = true;
			this.comB_chooseDiscipline.Location = new System.Drawing.Point(197, 44);
			this.comB_chooseDiscipline.Name = "comB_chooseDiscipline";
			this.comB_chooseDiscipline.Size = new System.Drawing.Size(207, 21);
			this.comB_chooseDiscipline.TabIndex = 0;
			this.comB_chooseDiscipline.SelectedIndexChanged += new System.EventHandler(this.comB_chooseDiscipline_SelectedIndexChanged);
			// 
			// page_testing
			// 
			this.page_testing.Controls.Add(this.b_nextQuestion);
			this.page_testing.Controls.Add(this.rb_var4);
			this.page_testing.Controls.Add(this.rb_var3);
			this.page_testing.Controls.Add(this.rb_var2);
			this.page_testing.Controls.Add(this.rb_var1);
			this.page_testing.Controls.Add(this.cb_var4);
			this.page_testing.Controls.Add(this.cb_var3);
			this.page_testing.Controls.Add(this.cb_var2);
			this.page_testing.Controls.Add(this.cb_var1);
			this.page_testing.Controls.Add(this.tb_questionText);
			this.page_testing.Location = new System.Drawing.Point(4, 22);
			this.page_testing.Name = "page_testing";
			this.page_testing.Padding = new System.Windows.Forms.Padding(3);
			this.page_testing.Size = new System.Drawing.Size(658, 421);
			this.page_testing.TabIndex = 1;
			this.page_testing.Text = "Тестування";
			this.page_testing.UseVisualStyleBackColor = true;
			// 
			// b_nextQuestion
			// 
			this.b_nextQuestion.Location = new System.Drawing.Point(538, 387);
			this.b_nextQuestion.Name = "b_nextQuestion";
			this.b_nextQuestion.Size = new System.Drawing.Size(75, 23);
			this.b_nextQuestion.TabIndex = 9;
			this.b_nextQuestion.Text = "Далі";
			this.b_nextQuestion.UseVisualStyleBackColor = true;
			this.b_nextQuestion.Click += new System.EventHandler(this.b_nextQuestion_Click);
			// 
			// rb_var4
			// 
			this.rb_var4.Location = new System.Drawing.Point(29, 319);
			this.rb_var4.Name = "rb_var4";
			this.rb_var4.Size = new System.Drawing.Size(601, 51);
			this.rb_var4.TabIndex = 8;
			this.rb_var4.TabStop = true;
			this.rb_var4.UseVisualStyleBackColor = true;
			this.rb_var4.Visible = false;
			// 
			// rb_var3
			// 
			this.rb_var3.Location = new System.Drawing.Point(29, 260);
			this.rb_var3.Name = "rb_var3";
			this.rb_var3.Size = new System.Drawing.Size(601, 51);
			this.rb_var3.TabIndex = 7;
			this.rb_var3.TabStop = true;
			this.rb_var3.UseVisualStyleBackColor = true;
			this.rb_var3.Visible = false;
			// 
			// rb_var2
			// 
			this.rb_var2.Location = new System.Drawing.Point(29, 201);
			this.rb_var2.Name = "rb_var2";
			this.rb_var2.Size = new System.Drawing.Size(601, 51);
			this.rb_var2.TabIndex = 6;
			this.rb_var2.TabStop = true;
			this.rb_var2.UseVisualStyleBackColor = true;
			this.rb_var2.Visible = false;
			// 
			// rb_var1
			// 
			this.rb_var1.Location = new System.Drawing.Point(29, 132);
			this.rb_var1.Name = "rb_var1";
			this.rb_var1.Size = new System.Drawing.Size(601, 61);
			this.rb_var1.TabIndex = 5;
			this.rb_var1.TabStop = true;
			this.rb_var1.UseVisualStyleBackColor = true;
			this.rb_var1.Visible = false;
			// 
			// cb_var4
			// 
			this.cb_var4.Location = new System.Drawing.Point(29, 319);
			this.cb_var4.Name = "cb_var4";
			this.cb_var4.Size = new System.Drawing.Size(601, 51);
			this.cb_var4.TabIndex = 4;
			this.cb_var4.UseVisualStyleBackColor = true;
			this.cb_var4.Visible = false;
			// 
			// cb_var3
			// 
			this.cb_var3.Location = new System.Drawing.Point(29, 260);
			this.cb_var3.Name = "cb_var3";
			this.cb_var3.Size = new System.Drawing.Size(601, 51);
			this.cb_var3.TabIndex = 3;
			this.cb_var3.UseVisualStyleBackColor = true;
			this.cb_var3.Visible = false;
			// 
			// cb_var2
			// 
			this.cb_var2.Location = new System.Drawing.Point(29, 201);
			this.cb_var2.Name = "cb_var2";
			this.cb_var2.Size = new System.Drawing.Size(601, 51);
			this.cb_var2.TabIndex = 2;
			this.cb_var2.UseVisualStyleBackColor = true;
			this.cb_var2.Visible = false;
			// 
			// cb_var1
			// 
			this.cb_var1.Location = new System.Drawing.Point(29, 132);
			this.cb_var1.Name = "cb_var1";
			this.cb_var1.Size = new System.Drawing.Size(601, 61);
			this.cb_var1.TabIndex = 1;
			this.cb_var1.UseVisualStyleBackColor = true;
			this.cb_var1.Visible = false;
			// 
			// tb_questionText
			// 
			this.tb_questionText.Enabled = false;
			this.tb_questionText.Location = new System.Drawing.Point(29, 16);
			this.tb_questionText.Multiline = true;
			this.tb_questionText.Name = "tb_questionText";
			this.tb_questionText.Size = new System.Drawing.Size(601, 108);
			this.tb_questionText.TabIndex = 0;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(666, 447);
			this.Controls.Add(this.tabControl1);
			this.Name = "MainForm";
			this.Text = "Форма тесту";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.tabControl1.ResumeLayout(false);
			this.page_chooseLab.ResumeLayout(false);
			this.page_chooseLab.PerformLayout();
			this.page_testing.ResumeLayout(false);
			this.page_testing.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage page_chooseLab;
		private System.Windows.Forms.TabPage page_testing;
		private System.Windows.Forms.Button b_startTesting;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comB_chooseLabWork;
		private System.Windows.Forms.ComboBox comB_chooseDiscipline;
		private System.Windows.Forms.Button b_nextQuestion;
		private System.Windows.Forms.RadioButton rb_var4;
		private System.Windows.Forms.RadioButton rb_var3;
		private System.Windows.Forms.RadioButton rb_var2;
		private System.Windows.Forms.RadioButton rb_var1;
		private System.Windows.Forms.CheckBox cb_var4;
		private System.Windows.Forms.CheckBox cb_var3;
		private System.Windows.Forms.CheckBox cb_var2;
		private System.Windows.Forms.CheckBox cb_var1;
		private System.Windows.Forms.TextBox tb_questionText;
	}
}