﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using DiplomHelper;
using DiplomHelper.Db;
using DiplomHelper.Entities;

namespace Diplom
{
	public partial class MainForm : BaseForm
	{
		private RadioButton[] _radioButtons = null;
		private CheckBox[] _checkBoxes = null;
		private Question[] _questions = null;
		private int _questionIndex = -1;
		private decimal _currentGrade = -1m;
		private static byte[] _key;

		public MainForm() {
			InitializeComponent();
			RbAndCbInitialize();
			_key = SHA256.Create().ComputeHash(Convert.FromBase64String(ConfigurationManager.AppSettings["additionalInfo"]));
		}

		public MainForm(string connectionString) : base(connectionString) {
			InitializeComponent();
			RbAndCbInitialize();
			_key = SHA256.Create().ComputeHash(Convert.FromBase64String(ConfigurationManager.AppSettings["additionalInfo"]));
		}

		private IEnumerable<Question> GetQuestions(Guid labWorkId) {
			var selectQuestion =
				new Select(_connectionString).Top(10)
					.Column<Guid>("Question", "Id")
					.Column<string>("Question", "Text")
					.Column<decimal>("Question", "Cost")
				.From("Question")
				.Where("Question", "LaboratoryWorkId").IsEqual(labWorkId)
				.OrderByRandom() as Select;
			DataTable table = selectQuestion.Execute();
			IEnumerable<Question> result = table.Rows.Cast<DataRow>()
				.Select(row => new Question((Guid)row[0], (string)row[1], labWorkId, (decimal)row[2]));
			return result.ToArray();
		}

		private void SetQuestionsAndAnswers(Guid labWorkId, byte[] key) {
			IEnumerable<Question> questions = GetQuestions(labWorkId);
			var select =
				new Select(_connectionString)
					.Column<Guid>("AnswerVariant", "Id")
					.Column<Guid>("AnswerVariant", "QuestionId")
					.Column<string>("AnswerVariant", "Text")
					.Column<bool>("AnswerVariant", "IsCorrect")
				.From("AnswerVariant")
				.Where("AnswerVariant", "QuestionId").In(questions.Select(q => (object)q.Id)) as Select;
			DataTable table = select.Execute();
			table.Rows.Cast<DataRow>()
				.Select(row => new AnswerVariant((Guid)row[0], Helper.DecryptString((string)row[2], key), (Guid)row[1], (bool)row[3]))
				.GroupBy(aw => aw.QuestionId)
				.ForEach(group => {
					Guid questId = group.Key;
					questions.First(q => q.Id == questId).Answers.AddRange(group);
				});
			_questions = (Question[])questions;
		}

		private void b_startTesting_Click(object sender, EventArgs e) {
			var currentLab = comB_chooseLabWork.SelectedItem as LaboratoryWork;
			if (currentLab is null) {
				MessageBox.Show("Необхідно обрати лабораторну роботу!",
					"Оберіть лабораторну роботу", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			SetQuestionsAndAnswers(currentLab.Id, (byte[])_key.Clone());
			_currentGrade = 0m;
			PrepareQuestion();
			(sender as Button).Enabled = false;
			tabControl1.SelectTab("page_testing");
		}

		private void PrepareQuestion() => PrepareQuestion(new Random());

		private void PrepareQuestion(Random random) {
			FlushRbAndCb();
			Question currentQuestion = null;
			try {
				currentQuestion = _questions[++_questionIndex];
			} catch (Exception) {
				Close();
				return;
			}
			tb_questionText.Text = currentQuestion.Text;
			AnswerVariant[] answers = currentQuestion.Answers.OrderBy(a => random.Next()).ToArray();
			int correctAnswers = answers.Count(a => a.IsCorrect);
			if (correctAnswers == 1) {
				for (int i = 0; i < answers.Length; i++) {
					_radioButtons[i].Text = answers[i].Text;
					_radioButtons[i].Visible = true;
					_checkBoxes[i].Text = answers[i].Id.ToString();
				}
			} else {
				for (int i = 0; i < answers.Length; i++) {
					_checkBoxes[i].Text = answers[i].Text;
					_checkBoxes[i].Visible = true;
					_radioButtons[i].Text = answers[i].Id.ToString();
				}
			}
		}

		private void CalculateAndSaveResult() {
			if (_questions is null || !_questions.Any()) {
				return;
			}
			Guid labWorkId = _questions.First().LabWorkId;
			decimal maxGrade = _questions.Sum(q => q.Cost);
			decimal percentGrade = 100m *_currentGrade / maxGrade;
			decimal roundedGrade = Math.Round(percentGrade, 2);
			var insert =
				new Insert(_connectionString).Into("SysStudentLabWorkGrades")
					.Set("StudentId", UserId)
					.Set("LaboratoryWorkId", labWorkId)
					.Set("Grade", Helper.EncryptString(roundedGrade.ToString(), (byte[])_key.Clone())) as Insert;
			insert.Execute();
			MessageBox.Show($"Ваш результат: {roundedGrade}%",
				"Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void page_chooseLab_Enter(object sender, EventArgs e) {
			Discipline[] disciplines = Helper.GetDisciplineList(_connectionString, UserId);
			comB_chooseDiscipline.Items.Clear();
			comB_chooseDiscipline.Items.AddRange(disciplines);
		}

		private void comB_chooseDiscipline_SelectedIndexChanged(object sender, EventArgs e) {
			var currentDiscipline = comB_chooseDiscipline.SelectedItem as Discipline;
			if (currentDiscipline is null) {
				comB_chooseLabWork.Items.Clear();
				return;
			}
			LaboratoryWork[] labs = Helper.GetLaboratoryWorks(_connectionString, currentDiscipline.Id);
			comB_chooseLabWork.Items.Clear();
			comB_chooseLabWork.Items.AddRange(labs);
		}

		private void b_nextQuestion_Click(object sender, EventArgs e) {
			decimal maxGrade = _questions.Sum(q => q.Cost);
			Question currentQuestion = _questions[_questionIndex];
			decimal maxGradePerQuestion = currentQuestion.Cost;
			AnswerVariant[] currentAnswers = currentQuestion.Answers.ToArray();
			int correctAnswersCount = currentAnswers.Count(a => a.IsCorrect);
			if (correctAnswersCount > 1) {
				if (_checkBoxes.Count(cb => cb.Checked) > correctAnswersCount) {
					_currentGrade += 0m;
				} else {
					var checkedAnswersIds = new List<Guid>();
					for (int i = 0; i < currentAnswers.Length; i++) {
						if (_checkBoxes[i].Checked) {
							checkedAnswersIds.Add(Guid.Parse(_radioButtons[i].Text));
						}
					}
					IEnumerable<AnswerVariant> selectedAnswers = currentAnswers
						.Where(a => checkedAnswersIds.Contains(a.Id));
					int selectedCorrectAnswersCount = selectedAnswers.Count(a => a.IsCorrect);
					_currentGrade += maxGradePerQuestion * selectedCorrectAnswersCount / correctAnswersCount;
				}
			} else {
				for (int i = 0; i < currentAnswers.Length; i++) {
					if (_radioButtons[i].Checked) {
						var selectedAnswerId = Guid.Parse(_checkBoxes[i].Text);
						AnswerVariant selectedAnswer = currentAnswers.First(a => a.Id == selectedAnswerId);
						if (selectedAnswer.IsCorrect) {
							_currentGrade += maxGradePerQuestion;
						}
						break;
					}
				}
			}
			PrepareQuestion();
		}

		private void RbAndCbInitialize() {
			_radioButtons = new[] {
				rb_var1,
				rb_var2,
				rb_var3,
				rb_var4
			};
			_checkBoxes = new[] {
				cb_var1,
				cb_var2,
				cb_var3,
				cb_var4
			};
			FlushRbAndCb();
		}

		private void FlushRbAndCb() {
			_radioButtons.ForEach(rb => {
				rb.Checked = rb.Visible = false;
				rb.Text = String.Empty;
			});
			_checkBoxes.ForEach(cb => {
				cb.Checked = cb.Visible = false;
				cb.Text = String.Empty;
			});
			tb_questionText.Text = String.Empty;
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e) => CalculateAndSaveResult();
	}
}
