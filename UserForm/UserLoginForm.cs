﻿using System;
using DiplomHelper;

namespace Diplom
{
	public partial class UserLoginForm : LoginForm
	{
		public UserLoginForm() : base() {
			InitializeComponent();
		}

		public UserLoginForm(string connectionString, Type mainFormType) : base(connectionString, mainFormType) {
			InitializeComponent();
		}

		protected override bool IsLoginGranted(LoginData loginData) => !loginData.IsAdmin;
	}
}
