﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;
using DiplomHelper.Db.CreateTables;

namespace Diplom
{
	static class Program
	{
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			string connectionString = ConfigurationManager.AppSettings["connectionString"];
			CreateTablesAndIndexes(connectionString);
			Application.Run(new AdminLoginForm(connectionString, typeof(MainForm)));
		}

		static void CreateTablesAndIndexes(string connectionString) {
			IEnumerable<CreateTablesHelper.CreateTableShell> tables = CreateTablesHelper.CreateTableShells;
			IEnumerable<CreateTablesHelper.CreateIndexShell> indexes = CreateTablesHelper.CreateIndexShells;
			using (var connection = new SqlConnection(connectionString)) {
				connection.Open();
				foreach (CreateTablesHelper.CreateTableShell table in tables) {
					string sql = String.Format(CreateTablesHelperResources.IsTableExistsSqlTextTemplate, table.TableName, table.CreateTableSqlStatement);
					using (var command = new SqlCommand(sql, connection)) {
						command.ExecuteNonQuery();
					}
				}
				foreach (CreateTablesHelper.CreateIndexShell index in indexes) {
					string sql = String.Format(CreateTablesHelperResources.IsIndexExistsSqlTextTemplate, index.IndexName, index.TableName, index.CreateIndexSqlStatement);
					using (var command = new SqlCommand(sql, connection)) {
						command.ExecuteNonQuery();
					}
				}
			}
		}
	}
}
