﻿using System;
using System.Windows.Forms;
using DiplomHelper;
using DiplomHelper.Db;
using DiplomHelper.Entities;
using DiplomHelper.Enums;

namespace Diplom.EntityForms
{
	public partial class LabWorkEntityForm : BaseEntityForm<LaboratoryWork>
	{
		public LabWorkEntityForm(BaseForm parent, string connectionString) :
				base(parent, connectionString) => InitializeComponent();

		private bool CheckIsLabWorkNumberExists(Guid disciplineId, int labWorkNumber) {
			var select =
				new Select(_connectionString)
					.Column<Guid>("Id")
				.From("LaboratoryWork")
				.Where("DisciplineId").IsEqual(disciplineId)
					.And("Number").IsEqual(labWorkNumber) as Select;
			return select.ExecuteScalar<Guid>() != default;
		}

		protected override void OnVisibleChanged(EventArgs e) {
			base.OnVisibleChanged(e);
			if (Visible && _state == State.Add) {
				Helper.LoadDisciplinesToComboBox(_connectionString, cb_disciplines);
			}
		}

		protected override void b_save_Click(object sender, EventArgs e) {
			string name = tb_name.Text.Trim();
			if (!Int32.TryParse(tb_number.Text, out int number)) {
				MessageBox.Show("Неправильно введено номер!", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (String.IsNullOrEmpty(name)) {
				MessageBox.Show("Необхідно ввести назву роботи!", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (_state == State.Add) {
				var discipline = cb_disciplines.SelectedItem as Discipline;
				if (discipline is null) {
					MessageBox.Show("Оберіть предмет, для котрого додається данна лабораторна робота!",
						"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				if (CheckIsLabWorkNumberExists(discipline.Id, number)) {
					MessageBox.Show("Цей предмет уже містить лабораторну роботу під цим номером", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				var insert =
					new Insert(_connectionString).Into("LaboratoryWork")
						.Set("Name", name)
						.Set("DisciplineId", discipline.Id)
						.Set("Number", number) as Insert;
				bool insertSuccessful = insert.Execute();
				if (!insertSuccessful) {
					MessageBox.Show("Не вдалося додати лабораторну роботу", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				MessageBox.Show("Лабораторну роботу створено", "Успіх",
						MessageBoxButtons.OK, MessageBoxIcon.Information);
			} else if (_state == State.Edit) {
				if (CheckIsLabWorkNumberExists(_entity.DisciplineId, number)) {
					MessageBox.Show("Цей предмет уже містить лабораторну роботу під цим номером", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				var update =
					new Update(_connectionString, "LaboratoryWork")
						.Set("Name", name)
						.Set("Number", number)
					.Where("Id").IsEqual(_entity.Id) as Update;
				update.Execute();
				MessageBox.Show("Лаборатну роботу відредаговано", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			Close();
		}

		public override void SetData(State state, LaboratoryWork entity = null) {
			base.SetData(state, entity);
			if (entity is null) {
				cb_disciplines.Visible = l_disciplines.Visible = true;
				cb_disciplines.SelectedItem = null;
				tb_name.Text = tb_number.Text = String.Empty;
				return;
			}
			cb_disciplines.Visible = l_disciplines.Visible = false;
			tb_name.Text = entity.Name;
			tb_number.Text = entity.Number.ToString();
		}
	}
}
