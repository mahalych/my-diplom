﻿using System;
using System.Globalization;
using System.Windows.Forms;
using DiplomHelper;
using DiplomHelper.Db;
using DiplomHelper.Entities;
using DiplomHelper.Enums;

namespace Diplom.EntityForms
{
	public partial class QuestionEntityForm : BaseEntityForm<Question>
	{
		private byte[] _key;

		public QuestionEntityForm(BaseForm parent, string connectionString, byte[] key) :
				base(parent, connectionString) {
			InitializeComponent();
			_key = key;
			SetEventHandlers();
		}

		private void SetEventHandlers() {
			tb_variant1.KeyUp += SetVariantVisibility;
			tb_variant2.KeyUp += SetVariantVisibility;
			tb_variant3.KeyUp += SetVariantVisibility;
			tb_variant4.KeyUp += SetVariantVisibility;
			tb_variant1.KeyUp += SetVarialLabelCBVisibility;
			tb_variant2.KeyUp += SetVarialLabelCBVisibility;
			tb_variant3.KeyUp += SetVarialLabelCBVisibility;
			tb_variant4.KeyUp += SetVarialLabelCBVisibility;
			cb_discipline.SelectedIndexChanged += cb_questionDiscipline_SelectedIndexChanged;
		}

		protected override void b_save_Click(object sender, EventArgs e) {
			Guid? labWorkId = (cb_labWork.SelectedItem as LaboratoryWork)?.Id;
			if (_state == State.Add && labWorkId is null) {
				MessageBox.Show("Оберіть лабораторну роботу!", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			string questionText = tb_questionText.Text.Trim();
			if (String.IsNullOrEmpty(questionText)) {
				MessageBox.Show("Уведіть текст запитання", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (!Decimal.TryParse(tb_questionCost.Text, out decimal cost) &&
					!Decimal.TryParse(tb_questionCost.Text, NumberStyles.Any,
						CultureInfo.InvariantCulture, out cost)) {
				MessageBox.Show("Вага питання повинна бути десятковим числом", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (String.IsNullOrWhiteSpace(tb_variant1.Text) &&
					String.IsNullOrWhiteSpace(tb_variant2.Text) &&
					String.IsNullOrWhiteSpace(tb_variant3.Text) &&
					String.IsNullOrWhiteSpace(tb_variant4.Text)) {
				MessageBox.Show("Уведіть текст хоча б одного варіанта відповіді", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (!cb_Variant1Correct.Checked &&
					!cb_Variant2Correct.Checked &&
					!cb_Variant3Correct.Checked &&
					!cb_Variant4Correct.Checked) {
				MessageBox.Show("Жоден варіант не відміченний як правильний", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (_state == State.Add) {
				var newQuestionId = Guid.NewGuid();
				try {
					InsertQuestion((Guid)labWorkId, questionText, cost, newQuestionId);
					InsertAnswerVariants(newQuestionId);
					MessageBox.Show("Запитання успішно додано", "Успіх",
						MessageBoxButtons.OK, MessageBoxIcon.Information);
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			} else if (_state == State.Edit) {
				Guid questionId = _entity.Id;
				var update =
					new Update(_connectionString, "Question")
						.Set("Text", questionText)
						.Set("Cost", cost)
					.Where("Id").IsEqual(questionId) as Update;
				update.Execute();
				var delete =
					new Delete(_connectionString, "AnswerVariant")
					.Where("QuestionId").IsEqual(questionId) as Delete;
				delete.Execute();
				InsertAnswerVariants(questionId);
				MessageBox.Show("Запитання відредаговано", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			Close();
		}

		private void InsertAnswerVariants(Guid newQuestionId) {
			AnswerVariantShell[] variants = new[] {
				new AnswerVariantShell {
					Text = tb_variant1.Text.Trim(),
					IsCorrect = cb_Variant1Correct.Checked
				},
				new AnswerVariantShell {
					Text = tb_variant2.Text.Trim(),
					IsCorrect = cb_Variant2Correct.Checked
				},
				new AnswerVariantShell {
					Text = tb_variant3.Text.Trim(),
					IsCorrect = cb_Variant3Correct.Checked
				},
				new AnswerVariantShell {
					Text = tb_variant4.Text.Trim(),
					IsCorrect = cb_Variant4Correct.Checked
				}
			};
			Helper.InsertAnswerVariants(_connectionString, newQuestionId, variants, (byte[])_key.Clone());
		}

		private void InsertQuestion(Guid labWorkId, string questionText, decimal cost, Guid newQuestionId) =>
			Helper.InsertQuestion(_connectionString, labWorkId, questionText, cost, newQuestionId);

		private void cb_questionDiscipline_SelectedIndexChanged(object sender, EventArgs e) {
			var currentDiscipline = cb_discipline.SelectedItem as Discipline;
			if (currentDiscipline is null) {
				cb_labWork.SelectedItem = null;
				cb_labWork.Items.Clear();
				return;
			}
			LaboratoryWork[] labs = Helper.GetLaboratoryWorks(_connectionString,
				currentDiscipline.Id);
			cb_labWork.Items.Clear();
			cb_labWork.Items.AddRange(labs);
		}

		private void SetVariantVisibility(object sender, EventArgs e) {
			string variant1 = tb_variant1.Text;
			string variant2 = tb_variant2.Text;
			string variant3 = tb_variant3.Text;
			bool var1Visible = true;
			bool var2Visible = var1Visible && !String.IsNullOrWhiteSpace(variant1);
			bool var3Visible = var2Visible && !String.IsNullOrWhiteSpace(variant2);
			bool var4Visible = var3Visible && !String.IsNullOrWhiteSpace(variant3);
			tb_variant1.Visible = var1Visible;
			tb_variant2.Visible = var2Visible;
			tb_variant3.Visible = var3Visible;
			tb_variant4.Visible = var4Visible;
			cb_Variant1Correct.Enabled = !String.IsNullOrEmpty(variant1);
			cb_Variant2Correct.Enabled = !String.IsNullOrEmpty(variant2);
			cb_Variant3Correct.Enabled = !String.IsNullOrEmpty(variant3);
			cb_Variant4Correct.Enabled = !String.IsNullOrEmpty(tb_variant4.Text);
		}

		private void SetVarialLabelCBVisibility(object sender, EventArgs e) {
			bool var1Visible = tb_variant1.Visible;
			bool var2Visible = tb_variant2.Visible;
			bool var3Visible = tb_variant3.Visible;
			bool var4Visible = tb_variant4.Visible;
			cb_Variant1Correct.Visible = l_variant1.Visible = var1Visible;
			cb_Variant2Correct.Visible = l_variant2.Visible = var2Visible;
			cb_Variant3Correct.Visible = l_variant3.Visible = var3Visible;
			cb_Variant4Correct.Visible = l_variant4.Visible = var4Visible;
			if (!var4Visible) {
				tb_variant4.Text = String.Empty;
			}
			if (!var3Visible) {
				tb_variant3.Text = String.Empty;
			}
			if (!var2Visible) {
				tb_variant2.Text = String.Empty;
			}
			if (!var1Visible) {
				tb_variant1.Text = String.Empty;
			}
		}

		private void LoadVariants(Guid questionId) {
			byte[] key = (byte[])_key.Clone();
			AnswerVariant[] variants = Helper.GetAnswerVariants(_connectionString, questionId);
			tb_variant4.Visible = l_variant4.Visible = cb_Variant4Correct.Visible = variants.Length >= 4;
			tb_variant3.Visible = l_variant3.Visible = cb_Variant3Correct.Visible = variants.Length >= 3;
			tb_variant2.Visible = l_variant2.Visible = cb_Variant2Correct.Visible = variants.Length >= 2;
			tb_variant1.Visible = l_variant1.Visible = cb_Variant1Correct.Visible = true;
			tb_variant1.Text = variants.Length >= 1 ? Helper.DecryptString(variants[0].Text, key) : String.Empty;
			cb_Variant1Correct.Checked = variants.Length >= 1 ? variants[0].IsCorrect : false;
			tb_variant2.Text = variants.Length >= 2 ? Helper.DecryptString(variants[1].Text, key) : String.Empty;
			cb_Variant2Correct.Checked = variants.Length >= 2 ? variants[1].IsCorrect : false;
			tb_variant3.Text = variants.Length >= 3 ? Helper.DecryptString(variants[2].Text, key) : String.Empty;
			cb_Variant3Correct.Checked = variants.Length >= 3 ? variants[2].IsCorrect : false;
			tb_variant4.Text = variants.Length >= 4 ? Helper.DecryptString(variants[3].Text, key) : String.Empty;
			cb_Variant4Correct.Checked = variants.Length >= 4 ? variants[3].IsCorrect : false;
		}

		public override void SetData(State state, Question entity = null) {
			base.SetData(state, entity);
			Helper.LoadDisciplinesToComboBox(_connectionString, cb_discipline);
			if (entity is null) {
				tb_questionCost.Text = tb_questionText.Text =
					tb_variant1.Text = tb_variant2.Text = tb_variant3.Text =
					tb_variant4.Text = String.Empty;
				l_discipline.Visible = l_labWork.Visible = cb_discipline.Visible = cb_labWork.Visible = true;
				return;
			}
			cb_discipline.SelectedItem = cb_labWork.SelectedItem = null;
			l_discipline.Visible = l_labWork.Visible = cb_discipline.Visible = cb_labWork.Visible = false;
			tb_questionText.Text = entity.Text;
			tb_questionCost.Text = entity.Cost.ToString();
			LoadVariants(entity.Id);
		}
	}
}
