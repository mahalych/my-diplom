﻿namespace Diplom.EntityForms
{
	partial class QuestionEntityForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.l_labWork = new System.Windows.Forms.Label();
			this.l_discipline = new System.Windows.Forms.Label();
			this.cb_labWork = new System.Windows.Forms.ComboBox();
			this.cb_discipline = new System.Windows.Forms.ComboBox();
			this.cb_Variant4Correct = new System.Windows.Forms.CheckBox();
			this.tb_variant4 = new System.Windows.Forms.TextBox();
			this.l_variant4 = new System.Windows.Forms.Label();
			this.cb_Variant3Correct = new System.Windows.Forms.CheckBox();
			this.tb_variant3 = new System.Windows.Forms.TextBox();
			this.l_variant3 = new System.Windows.Forms.Label();
			this.cb_Variant2Correct = new System.Windows.Forms.CheckBox();
			this.tb_variant2 = new System.Windows.Forms.TextBox();
			this.l_variant2 = new System.Windows.Forms.Label();
			this.cb_Variant1Correct = new System.Windows.Forms.CheckBox();
			this.tb_variant1 = new System.Windows.Forms.TextBox();
			this.l_variant1 = new System.Windows.Forms.Label();
			this.tb_questionCost = new System.Windows.Forms.TextBox();
			this.l_questionCost = new System.Windows.Forms.Label();
			this.tb_questionText = new System.Windows.Forms.TextBox();
			this.l_questionText = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// b_cancel
			// 
			this.b_cancel.Location = new System.Drawing.Point(585, 175);
			// 
			// b_save
			// 
			this.b_save.Location = new System.Drawing.Point(487, 175);
			// 
			// l_labWork
			// 
			this.l_labWork.AutoSize = true;
			this.l_labWork.Location = new System.Drawing.Point(397, 92);
			this.l_labWork.Name = "l_labWork";
			this.l_labWork.Size = new System.Drawing.Size(112, 13);
			this.l_labWork.TabIndex = 41;
			this.l_labWork.Text = "Лабораторна робота";
			this.l_labWork.Visible = false;
			// 
			// l_discipline
			// 
			this.l_discipline.AutoSize = true;
			this.l_discipline.Location = new System.Drawing.Point(457, 48);
			this.l_discipline.Name = "l_discipline";
			this.l_discipline.Size = new System.Drawing.Size(52, 13);
			this.l_discipline.TabIndex = 40;
			this.l_discipline.Text = "Предмет";
			this.l_discipline.Visible = false;
			// 
			// cb_labWork
			// 
			this.cb_labWork.FormattingEnabled = true;
			this.cb_labWork.Location = new System.Drawing.Point(515, 89);
			this.cb_labWork.Name = "cb_labWork";
			this.cb_labWork.Size = new System.Drawing.Size(191, 21);
			this.cb_labWork.TabIndex = 39;
			this.cb_labWork.Visible = false;
			// 
			// cb_discipline
			// 
			this.cb_discipline.FormattingEnabled = true;
			this.cb_discipline.Location = new System.Drawing.Point(515, 45);
			this.cb_discipline.Name = "cb_discipline";
			this.cb_discipline.Size = new System.Drawing.Size(191, 21);
			this.cb_discipline.TabIndex = 38;
			this.cb_discipline.Visible = false;
			// 
			// cb_Variant4Correct
			// 
			this.cb_Variant4Correct.AutoSize = true;
			this.cb_Variant4Correct.Location = new System.Drawing.Point(288, 287);
			this.cb_Variant4Correct.Name = "cb_Variant4Correct";
			this.cb_Variant4Correct.Size = new System.Drawing.Size(88, 17);
			this.cb_Variant4Correct.TabIndex = 37;
			this.cb_Variant4Correct.Text = "Правильний";
			this.cb_Variant4Correct.UseVisualStyleBackColor = true;
			this.cb_Variant4Correct.Visible = false;
			// 
			// tb_variant4
			// 
			this.tb_variant4.Location = new System.Drawing.Point(106, 288);
			this.tb_variant4.Multiline = true;
			this.tb_variant4.Name = "tb_variant4";
			this.tb_variant4.Size = new System.Drawing.Size(176, 47);
			this.tb_variant4.TabIndex = 36;
			this.tb_variant4.Visible = false;
			// 
			// l_variant4
			// 
			this.l_variant4.AutoSize = true;
			this.l_variant4.Location = new System.Drawing.Point(45, 306);
			this.l_variant4.Name = "l_variant4";
			this.l_variant4.Size = new System.Drawing.Size(54, 13);
			this.l_variant4.TabIndex = 35;
			this.l_variant4.Text = "Варіант 4";
			this.l_variant4.Visible = false;
			// 
			// cb_Variant3Correct
			// 
			this.cb_Variant3Correct.AutoSize = true;
			this.cb_Variant3Correct.Location = new System.Drawing.Point(288, 234);
			this.cb_Variant3Correct.Name = "cb_Variant3Correct";
			this.cb_Variant3Correct.Size = new System.Drawing.Size(88, 17);
			this.cb_Variant3Correct.TabIndex = 34;
			this.cb_Variant3Correct.Text = "Правильний";
			this.cb_Variant3Correct.UseVisualStyleBackColor = true;
			this.cb_Variant3Correct.Visible = false;
			// 
			// tb_variant3
			// 
			this.tb_variant3.Location = new System.Drawing.Point(106, 235);
			this.tb_variant3.Multiline = true;
			this.tb_variant3.Name = "tb_variant3";
			this.tb_variant3.Size = new System.Drawing.Size(176, 47);
			this.tb_variant3.TabIndex = 33;
			this.tb_variant3.Visible = false;
			// 
			// l_variant3
			// 
			this.l_variant3.AutoSize = true;
			this.l_variant3.Location = new System.Drawing.Point(45, 253);
			this.l_variant3.Name = "l_variant3";
			this.l_variant3.Size = new System.Drawing.Size(54, 13);
			this.l_variant3.TabIndex = 32;
			this.l_variant3.Text = "Варіант 3";
			this.l_variant3.Visible = false;
			// 
			// cb_Variant2Correct
			// 
			this.cb_Variant2Correct.AutoSize = true;
			this.cb_Variant2Correct.Location = new System.Drawing.Point(288, 181);
			this.cb_Variant2Correct.Name = "cb_Variant2Correct";
			this.cb_Variant2Correct.Size = new System.Drawing.Size(88, 17);
			this.cb_Variant2Correct.TabIndex = 31;
			this.cb_Variant2Correct.Text = "Правильний";
			this.cb_Variant2Correct.UseVisualStyleBackColor = true;
			this.cb_Variant2Correct.Visible = false;
			// 
			// tb_variant2
			// 
			this.tb_variant2.Location = new System.Drawing.Point(106, 182);
			this.tb_variant2.Multiline = true;
			this.tb_variant2.Name = "tb_variant2";
			this.tb_variant2.Size = new System.Drawing.Size(176, 47);
			this.tb_variant2.TabIndex = 30;
			this.tb_variant2.Visible = false;
			// 
			// l_variant2
			// 
			this.l_variant2.AutoSize = true;
			this.l_variant2.Location = new System.Drawing.Point(45, 200);
			this.l_variant2.Name = "l_variant2";
			this.l_variant2.Size = new System.Drawing.Size(54, 13);
			this.l_variant2.TabIndex = 29;
			this.l_variant2.Text = "Варіант 2";
			this.l_variant2.Visible = false;
			// 
			// cb_Variant1Correct
			// 
			this.cb_Variant1Correct.AutoSize = true;
			this.cb_Variant1Correct.Location = new System.Drawing.Point(288, 128);
			this.cb_Variant1Correct.Name = "cb_Variant1Correct";
			this.cb_Variant1Correct.Size = new System.Drawing.Size(88, 17);
			this.cb_Variant1Correct.TabIndex = 28;
			this.cb_Variant1Correct.Text = "Правильний";
			this.cb_Variant1Correct.UseVisualStyleBackColor = true;
			// 
			// tb_variant1
			// 
			this.tb_variant1.Location = new System.Drawing.Point(106, 129);
			this.tb_variant1.Multiline = true;
			this.tb_variant1.Name = "tb_variant1";
			this.tb_variant1.Size = new System.Drawing.Size(176, 47);
			this.tb_variant1.TabIndex = 27;
			// 
			// l_variant1
			// 
			this.l_variant1.AutoSize = true;
			this.l_variant1.Location = new System.Drawing.Point(45, 147);
			this.l_variant1.Name = "l_variant1";
			this.l_variant1.Size = new System.Drawing.Size(54, 13);
			this.l_variant1.TabIndex = 26;
			this.l_variant1.Text = "Варіант 1";
			// 
			// tb_questionCost
			// 
			this.tb_questionCost.Location = new System.Drawing.Point(131, 82);
			this.tb_questionCost.Name = "tb_questionCost";
			this.tb_questionCost.Size = new System.Drawing.Size(100, 20);
			this.tb_questionCost.TabIndex = 25;
			this.tb_questionCost.Text = "5";
			// 
			// l_questionCost
			// 
			this.l_questionCost.AutoSize = true;
			this.l_questionCost.Location = new System.Drawing.Point(50, 85);
			this.l_questionCost.Name = "l_questionCost";
			this.l_questionCost.Size = new System.Drawing.Size(75, 13);
			this.l_questionCost.TabIndex = 24;
			this.l_questionCost.Text = "Вага питання";
			// 
			// tb_questionText
			// 
			this.tb_questionText.Location = new System.Drawing.Point(131, 25);
			this.tb_questionText.Multiline = true;
			this.tb_questionText.Name = "tb_questionText";
			this.tb_questionText.Size = new System.Drawing.Size(223, 51);
			this.tb_questionText.TabIndex = 23;
			// 
			// l_questionText
			// 
			this.l_questionText.AutoSize = true;
			this.l_questionText.Location = new System.Drawing.Point(44, 50);
			this.l_questionText.Name = "l_questionText";
			this.l_questionText.Size = new System.Drawing.Size(81, 13);
			this.l_questionText.TabIndex = 22;
			this.l_questionText.Text = "Текст питання";
			// 
			// QuestionEntityForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(747, 371);
			this.Controls.Add(this.l_labWork);
			this.Controls.Add(this.l_discipline);
			this.Controls.Add(this.cb_labWork);
			this.Controls.Add(this.cb_discipline);
			this.Controls.Add(this.cb_Variant4Correct);
			this.Controls.Add(this.tb_variant4);
			this.Controls.Add(this.l_variant4);
			this.Controls.Add(this.cb_Variant3Correct);
			this.Controls.Add(this.tb_variant3);
			this.Controls.Add(this.l_variant3);
			this.Controls.Add(this.cb_Variant2Correct);
			this.Controls.Add(this.tb_variant2);
			this.Controls.Add(this.l_variant2);
			this.Controls.Add(this.cb_Variant1Correct);
			this.Controls.Add(this.tb_variant1);
			this.Controls.Add(this.l_variant1);
			this.Controls.Add(this.tb_questionCost);
			this.Controls.Add(this.l_questionCost);
			this.Controls.Add(this.tb_questionText);
			this.Controls.Add(this.l_questionText);
			this.Name = "QuestionEntityForm";
			this.Text = "Запитання";
			this.Controls.SetChildIndex(this.l_questionText, 0);
			this.Controls.SetChildIndex(this.tb_questionText, 0);
			this.Controls.SetChildIndex(this.l_questionCost, 0);
			this.Controls.SetChildIndex(this.tb_questionCost, 0);
			this.Controls.SetChildIndex(this.l_variant1, 0);
			this.Controls.SetChildIndex(this.tb_variant1, 0);
			this.Controls.SetChildIndex(this.cb_Variant1Correct, 0);
			this.Controls.SetChildIndex(this.l_variant2, 0);
			this.Controls.SetChildIndex(this.tb_variant2, 0);
			this.Controls.SetChildIndex(this.cb_Variant2Correct, 0);
			this.Controls.SetChildIndex(this.l_variant3, 0);
			this.Controls.SetChildIndex(this.tb_variant3, 0);
			this.Controls.SetChildIndex(this.cb_Variant3Correct, 0);
			this.Controls.SetChildIndex(this.l_variant4, 0);
			this.Controls.SetChildIndex(this.tb_variant4, 0);
			this.Controls.SetChildIndex(this.cb_Variant4Correct, 0);
			this.Controls.SetChildIndex(this.cb_discipline, 0);
			this.Controls.SetChildIndex(this.cb_labWork, 0);
			this.Controls.SetChildIndex(this.l_discipline, 0);
			this.Controls.SetChildIndex(this.l_labWork, 0);
			this.Controls.SetChildIndex(this.b_save, 0);
			this.Controls.SetChildIndex(this.b_cancel, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label l_labWork;
		private System.Windows.Forms.Label l_discipline;
		private System.Windows.Forms.ComboBox cb_labWork;
		private System.Windows.Forms.ComboBox cb_discipline;
		private System.Windows.Forms.CheckBox cb_Variant4Correct;
		private System.Windows.Forms.TextBox tb_variant4;
		private System.Windows.Forms.Label l_variant4;
		private System.Windows.Forms.CheckBox cb_Variant3Correct;
		private System.Windows.Forms.TextBox tb_variant3;
		private System.Windows.Forms.Label l_variant3;
		private System.Windows.Forms.CheckBox cb_Variant2Correct;
		private System.Windows.Forms.TextBox tb_variant2;
		private System.Windows.Forms.Label l_variant2;
		private System.Windows.Forms.CheckBox cb_Variant1Correct;
		private System.Windows.Forms.TextBox tb_variant1;
		private System.Windows.Forms.Label l_variant1;
		private System.Windows.Forms.TextBox tb_questionCost;
		private System.Windows.Forms.Label l_questionCost;
		private System.Windows.Forms.TextBox tb_questionText;
		private System.Windows.Forms.Label l_questionText;
	}
}