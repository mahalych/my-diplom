﻿namespace Diplom.EntityForms
{
	partial class DisciplineEntityForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.tb_name = new System.Windows.Forms.TextBox();
			this.l_name = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// b_cancel
			// 
			this.b_cancel.Location = new System.Drawing.Point(185, 102);
			// 
			// b_save
			// 
			this.b_save.Location = new System.Drawing.Point(46, 102);
			// 
			// tb_name
			// 
			this.tb_name.Location = new System.Drawing.Point(102, 40);
			this.tb_name.Name = "tb_name";
			this.tb_name.Size = new System.Drawing.Size(184, 20);
			this.tb_name.TabIndex = 28;
			// 
			// l_name
			// 
			this.l_name.AutoSize = true;
			this.l_name.Location = new System.Drawing.Point(43, 43);
			this.l_name.Name = "l_name";
			this.l_name.Size = new System.Drawing.Size(39, 13);
			this.l_name.TabIndex = 27;
			this.l_name.Text = "Назва";
			// 
			// DisciplineEntityForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(347, 201);
			this.Controls.Add(this.tb_name);
			this.Controls.Add(this.l_name);
			this.Name = "DisciplineEntityForm";
			this.Text = "Предмет";
			this.Controls.SetChildIndex(this.b_save, 0);
			this.Controls.SetChildIndex(this.b_cancel, 0);
			this.Controls.SetChildIndex(this.l_name, 0);
			this.Controls.SetChildIndex(this.tb_name, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tb_name;
		private System.Windows.Forms.Label l_name;
	}
}