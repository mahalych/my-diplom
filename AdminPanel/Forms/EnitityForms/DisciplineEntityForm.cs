﻿using System;
using System.Windows.Forms;
using DiplomHelper;
using DiplomHelper.Db;
using DiplomHelper.Entities;
using DiplomHelper.Enums;

namespace Diplom.EntityForms
{
	public partial class DisciplineEntityForm : BaseEntityForm<Discipline>
	{
		public DisciplineEntityForm(BaseForm parent, string connectionString) :
				base(parent, connectionString) => InitializeComponent();

		protected override void b_save_Click(object sender, EventArgs e) {
			string newDisciplineName = tb_name.Text.Trim();
			if (String.IsNullOrEmpty(newDisciplineName)) {
				MessageBox.Show("Вкажіть назву предмету", "Не вказана назва предмету",
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (CheckIsDisciplineExists(newDisciplineName)) {
				MessageBox.Show("Предмет з такою назвою вже існує",
					"Повторювання назви", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (_state == State.Add) {
				var insert =
					new Insert(_connectionString).Into("Discipline")
						.Set("Name", newDisciplineName) as Insert;
				bool insertSuccess = insert.Execute();
				if (!insertSuccess) {
					MessageBox.Show("Не вдалося додати предмет", "Помилка",
						MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				MessageBox.Show("Предмет додано", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			} else if (_state == State.Edit) {
				var update =
					new Update(_connectionString, "Discipline")
						.Set("Name", newDisciplineName)
					.Where("Id").IsEqual(_entity.Id) as Update;
				update.Execute();
				MessageBox.Show("Предмет відредаговано", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			Close();
		}

		private bool CheckIsDisciplineExists(string name) {
			var select =
				new Select(_connectionString)
					.Column<Guid>("Id")
				.From("Discipline")
				.Where(new Upper("Name")).IsEqual(name.ToUpper()) as Select;
			return select.ExecuteScalar<Guid>() != default;
		}

		public override void SetData(State state, Discipline entity = null) {
			base.SetData(state, entity);
			if (entity is null) {
				tb_name.Text = String.Empty;
				return;
			}
			tb_name.Text = entity.Name;
		}
	}
}
