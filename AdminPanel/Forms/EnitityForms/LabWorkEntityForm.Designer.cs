﻿namespace Diplom.EntityForms
{
	partial class LabWorkEntityForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.l_disciplines = new System.Windows.Forms.Label();
			this.cb_disciplines = new System.Windows.Forms.ComboBox();
			this.tb_number = new System.Windows.Forms.TextBox();
			this.l_number = new System.Windows.Forms.Label();
			this.tb_name = new System.Windows.Forms.TextBox();
			this.l_name = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// b_cancel
			// 
			this.b_cancel.Location = new System.Drawing.Point(177, 185);
			// 
			// b_save
			// 
			this.b_save.Location = new System.Drawing.Point(60, 185);
			// 
			// l_disciplines
			// 
			this.l_disciplines.AutoSize = true;
			this.l_disciplines.Location = new System.Drawing.Point(12, 39);
			this.l_disciplines.Name = "l_disciplines";
			this.l_disciplines.Size = new System.Drawing.Size(52, 13);
			this.l_disciplines.TabIndex = 14;
			this.l_disciplines.Text = "Предмет";
			this.l_disciplines.Visible = false;
			// 
			// cb_disciplines
			// 
			this.cb_disciplines.FormattingEnabled = true;
			this.cb_disciplines.Location = new System.Drawing.Point(95, 36);
			this.cb_disciplines.Name = "cb_disciplines";
			this.cb_disciplines.Size = new System.Drawing.Size(205, 21);
			this.cb_disciplines.TabIndex = 13;
			this.cb_disciplines.Visible = false;
			// 
			// tb_number
			// 
			this.tb_number.Location = new System.Drawing.Point(95, 120);
			this.tb_number.Name = "tb_number";
			this.tb_number.Size = new System.Drawing.Size(100, 20);
			this.tb_number.TabIndex = 12;
			// 
			// l_number
			// 
			this.l_number.AutoSize = true;
			this.l_number.Location = new System.Drawing.Point(12, 123);
			this.l_number.Name = "l_number";
			this.l_number.Size = new System.Drawing.Size(41, 13);
			this.l_number.TabIndex = 11;
			this.l_number.Text = "Номер";
			// 
			// tb_name
			// 
			this.tb_name.Location = new System.Drawing.Point(95, 73);
			this.tb_name.Name = "tb_name";
			this.tb_name.Size = new System.Drawing.Size(205, 20);
			this.tb_name.TabIndex = 10;
			// 
			// l_name
			// 
			this.l_name.AutoSize = true;
			this.l_name.Location = new System.Drawing.Point(12, 80);
			this.l_name.Name = "l_name";
			this.l_name.Size = new System.Drawing.Size(39, 13);
			this.l_name.TabIndex = 9;
			this.l_name.Text = "Назва";
			// 
			// LabWorkEntityForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(326, 251);
			this.Controls.Add(this.l_disciplines);
			this.Controls.Add(this.cb_disciplines);
			this.Controls.Add(this.tb_number);
			this.Controls.Add(this.l_number);
			this.Controls.Add(this.tb_name);
			this.Controls.Add(this.l_name);
			this.Name = "LabWorkEntityForm";
			this.Text = "Лабораторна робота";
			this.Controls.SetChildIndex(this.l_name, 0);
			this.Controls.SetChildIndex(this.tb_name, 0);
			this.Controls.SetChildIndex(this.l_number, 0);
			this.Controls.SetChildIndex(this.tb_number, 0);
			this.Controls.SetChildIndex(this.cb_disciplines, 0);
			this.Controls.SetChildIndex(this.l_disciplines, 0);
			this.Controls.SetChildIndex(this.b_save, 0);
			this.Controls.SetChildIndex(this.b_cancel, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label l_disciplines;
		private System.Windows.Forms.ComboBox cb_disciplines;
		private System.Windows.Forms.TextBox tb_number;
		private System.Windows.Forms.Label l_number;
		private System.Windows.Forms.TextBox tb_name;
		private System.Windows.Forms.Label l_name;
	}
}