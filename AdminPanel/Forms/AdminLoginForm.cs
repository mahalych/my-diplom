﻿using System;
using DiplomHelper;

namespace Diplom
{
	public partial class AdminLoginForm : LoginForm
	{
		public AdminLoginForm() : base() => InitializeComponent();

		public AdminLoginForm(string connectionString, Type mainFormType)
			: base(connectionString, mainFormType) => InitializeComponent();

		public override bool IsAdminForm => true;

		protected override bool IsLoginGranted(LoginData loginData) => loginData.IsAdmin;
	}
}
