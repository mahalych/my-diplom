﻿namespace Diplom
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.page_user = new System.Windows.Forms.TabPage();
			this.b_deleteUser = new System.Windows.Forms.Button();
			this.cb_userSelector = new System.Windows.Forms.ComboBox();
			this.b_editUser = new System.Windows.Forms.Button();
			this.b_createUser = new System.Windows.Forms.Button();
			this.page_discipline = new System.Windows.Forms.TabPage();
			this.b_deleteDiscipline = new System.Windows.Forms.Button();
			this.cb_disciplineSelector = new System.Windows.Forms.ComboBox();
			this.b_editDiscipline = new System.Windows.Forms.Button();
			this.b_createDiscipline = new System.Windows.Forms.Button();
			this.page_createLab = new System.Windows.Forms.TabPage();
			this.b_deleteLabWork = new System.Windows.Forms.Button();
			this.l_labWorkSelector = new System.Windows.Forms.Label();
			this.l_labWorkDisciplineSelector = new System.Windows.Forms.Label();
			this.cb_labWorkDisciplineSelector = new System.Windows.Forms.ComboBox();
			this.cb_labWorkSelector = new System.Windows.Forms.ComboBox();
			this.b_editLabWork = new System.Windows.Forms.Button();
			this.b_createLabWork = new System.Windows.Forms.Button();
			this.page_question = new System.Windows.Forms.TabPage();
			this.b_deleteQuestion = new System.Windows.Forms.Button();
			this.cb_questionSelector = new System.Windows.Forms.ComboBox();
			this.cb_questionLabWorkSelector = new System.Windows.Forms.ComboBox();
			this.cb_questionDisciplineSelector = new System.Windows.Forms.ComboBox();
			this.l_questionSelector = new System.Windows.Forms.Label();
			this.l_questionLabWorkSelector = new System.Windows.Forms.Label();
			this.l_questionDisciplineSelector = new System.Windows.Forms.Label();
			this.b_editQuestion = new System.Windows.Forms.Button();
			this.b_createQuestion = new System.Windows.Forms.Button();
			this.page_results = new System.Windows.Forms.TabPage();
			this.b_downloadCsv = new System.Windows.Forms.Button();
			this.gb_resultFilters = new System.Windows.Forms.GroupBox();
			this.cb_descSort = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cb_filterLabWork = new System.Windows.Forms.ComboBox();
			this.cb_filterDiscipline = new System.Windows.Forms.ComboBox();
			this.dtp_filterEndDate = new System.Windows.Forms.DateTimePicker();
			this.dtp_filterStartDate = new System.Windows.Forms.DateTimePicker();
			this.cb_filterStudent = new System.Windows.Forms.ComboBox();
			this.b_downloadJson = new System.Windows.Forms.Button();
			this.b_downloadXml = new System.Windows.Forms.Button();
			this.b_quickResult = new System.Windows.Forms.Button();
			this.page_import = new System.Windows.Forms.TabPage();
			this.b_importXml = new System.Windows.Forms.Button();
			this.b_importJson = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.tabControl1.SuspendLayout();
			this.page_user.SuspendLayout();
			this.page_discipline.SuspendLayout();
			this.page_createLab.SuspendLayout();
			this.page_question.SuspendLayout();
			this.page_results.SuspendLayout();
			this.gb_resultFilters.SuspendLayout();
			this.page_import.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.page_user);
			this.tabControl1.Controls.Add(this.page_discipline);
			this.tabControl1.Controls.Add(this.page_createLab);
			this.tabControl1.Controls.Add(this.page_question);
			this.tabControl1.Controls.Add(this.page_results);
			this.tabControl1.Controls.Add(this.page_import);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(468, 267);
			this.tabControl1.TabIndex = 0;
			// 
			// page_user
			// 
			this.page_user.Controls.Add(this.label7);
			this.page_user.Controls.Add(this.b_deleteUser);
			this.page_user.Controls.Add(this.cb_userSelector);
			this.page_user.Controls.Add(this.b_editUser);
			this.page_user.Controls.Add(this.b_createUser);
			this.page_user.Location = new System.Drawing.Point(4, 22);
			this.page_user.Name = "page_user";
			this.page_user.Padding = new System.Windows.Forms.Padding(3);
			this.page_user.Size = new System.Drawing.Size(460, 241);
			this.page_user.TabIndex = 0;
			this.page_user.Text = "Студент";
			this.page_user.UseVisualStyleBackColor = true;
			this.page_user.Enter += new System.EventHandler(this.page_user_Enter);
			// 
			// b_deleteUser
			// 
			this.b_deleteUser.Location = new System.Drawing.Point(261, 146);
			this.b_deleteUser.Name = "b_deleteUser";
			this.b_deleteUser.Size = new System.Drawing.Size(75, 23);
			this.b_deleteUser.TabIndex = 12;
			this.b_deleteUser.Text = "Видалити";
			this.b_deleteUser.UseVisualStyleBackColor = true;
			this.b_deleteUser.Click += new System.EventHandler(this.b_deleteUser_Click);
			// 
			// cb_userSelector
			// 
			this.cb_userSelector.FormattingEnabled = true;
			this.cb_userSelector.Location = new System.Drawing.Point(141, 100);
			this.cb_userSelector.Name = "cb_userSelector";
			this.cb_userSelector.Size = new System.Drawing.Size(195, 21);
			this.cb_userSelector.TabIndex = 10;
			// 
			// b_editUser
			// 
			this.b_editUser.Location = new System.Drawing.Point(141, 146);
			this.b_editUser.Name = "b_editUser";
			this.b_editUser.Size = new System.Drawing.Size(75, 23);
			this.b_editUser.TabIndex = 9;
			this.b_editUser.Text = "Змінити";
			this.b_editUser.UseVisualStyleBackColor = true;
			this.b_editUser.Click += new System.EventHandler(this.b_editUser_Click);
			// 
			// b_createUser
			// 
			this.b_createUser.Location = new System.Drawing.Point(197, 31);
			this.b_createUser.Name = "b_createUser";
			this.b_createUser.Size = new System.Drawing.Size(75, 23);
			this.b_createUser.TabIndex = 8;
			this.b_createUser.Text = "Створити";
			this.b_createUser.UseVisualStyleBackColor = true;
			this.b_createUser.Click += new System.EventHandler(this.b_createUser_Click);
			// 
			// page_discipline
			// 
			this.page_discipline.Controls.Add(this.label6);
			this.page_discipline.Controls.Add(this.b_deleteDiscipline);
			this.page_discipline.Controls.Add(this.cb_disciplineSelector);
			this.page_discipline.Controls.Add(this.b_editDiscipline);
			this.page_discipline.Controls.Add(this.b_createDiscipline);
			this.page_discipline.Location = new System.Drawing.Point(4, 22);
			this.page_discipline.Name = "page_discipline";
			this.page_discipline.Padding = new System.Windows.Forms.Padding(3);
			this.page_discipline.Size = new System.Drawing.Size(460, 241);
			this.page_discipline.TabIndex = 3;
			this.page_discipline.Text = "Предмет";
			this.page_discipline.UseVisualStyleBackColor = true;
			this.page_discipline.Enter += new System.EventHandler(this.page_discipline_Enter);
			// 
			// b_deleteDiscipline
			// 
			this.b_deleteDiscipline.Location = new System.Drawing.Point(268, 137);
			this.b_deleteDiscipline.Name = "b_deleteDiscipline";
			this.b_deleteDiscipline.Size = new System.Drawing.Size(75, 23);
			this.b_deleteDiscipline.TabIndex = 7;
			this.b_deleteDiscipline.Text = "Видалити";
			this.b_deleteDiscipline.UseVisualStyleBackColor = true;
			this.b_deleteDiscipline.Click += new System.EventHandler(this.b_deleteDiscipline_Click);
			// 
			// cb_disciplineSelector
			// 
			this.cb_disciplineSelector.FormattingEnabled = true;
			this.cb_disciplineSelector.Location = new System.Drawing.Point(117, 77);
			this.cb_disciplineSelector.Name = "cb_disciplineSelector";
			this.cb_disciplineSelector.Size = new System.Drawing.Size(226, 21);
			this.cb_disciplineSelector.TabIndex = 6;
			// 
			// b_editDiscipline
			// 
			this.b_editDiscipline.Location = new System.Drawing.Point(117, 137);
			this.b_editDiscipline.Name = "b_editDiscipline";
			this.b_editDiscipline.Size = new System.Drawing.Size(75, 23);
			this.b_editDiscipline.TabIndex = 4;
			this.b_editDiscipline.Text = "Змінити";
			this.b_editDiscipline.UseVisualStyleBackColor = true;
			this.b_editDiscipline.Click += new System.EventHandler(this.b_editDiscipline_Click);
			// 
			// b_createDiscipline
			// 
			this.b_createDiscipline.Location = new System.Drawing.Point(195, 29);
			this.b_createDiscipline.Name = "b_createDiscipline";
			this.b_createDiscipline.Size = new System.Drawing.Size(75, 23);
			this.b_createDiscipline.TabIndex = 3;
			this.b_createDiscipline.Text = "Створити";
			this.b_createDiscipline.UseVisualStyleBackColor = true;
			this.b_createDiscipline.Click += new System.EventHandler(this.b_createDiscipline_Click);
			// 
			// page_createLab
			// 
			this.page_createLab.Controls.Add(this.b_deleteLabWork);
			this.page_createLab.Controls.Add(this.l_labWorkSelector);
			this.page_createLab.Controls.Add(this.l_labWorkDisciplineSelector);
			this.page_createLab.Controls.Add(this.cb_labWorkDisciplineSelector);
			this.page_createLab.Controls.Add(this.cb_labWorkSelector);
			this.page_createLab.Controls.Add(this.b_editLabWork);
			this.page_createLab.Controls.Add(this.b_createLabWork);
			this.page_createLab.Location = new System.Drawing.Point(4, 22);
			this.page_createLab.Name = "page_createLab";
			this.page_createLab.Padding = new System.Windows.Forms.Padding(3);
			this.page_createLab.Size = new System.Drawing.Size(460, 241);
			this.page_createLab.TabIndex = 2;
			this.page_createLab.Text = "Лабораторна робота";
			this.page_createLab.UseVisualStyleBackColor = true;
			this.page_createLab.Enter += new System.EventHandler(this.page_lab_Enter);
			// 
			// b_deleteLabWork
			// 
			this.b_deleteLabWork.Location = new System.Drawing.Point(304, 147);
			this.b_deleteLabWork.Name = "b_deleteLabWork";
			this.b_deleteLabWork.Size = new System.Drawing.Size(75, 23);
			this.b_deleteLabWork.TabIndex = 16;
			this.b_deleteLabWork.Text = "Видалити";
			this.b_deleteLabWork.UseVisualStyleBackColor = true;
			this.b_deleteLabWork.Click += new System.EventHandler(this.b_deleteLabWork_Click);
			// 
			// l_labWorkSelector
			// 
			this.l_labWorkSelector.AutoSize = true;
			this.l_labWorkSelector.Location = new System.Drawing.Point(71, 105);
			this.l_labWorkSelector.Name = "l_labWorkSelector";
			this.l_labWorkSelector.Size = new System.Drawing.Size(112, 13);
			this.l_labWorkSelector.TabIndex = 15;
			this.l_labWorkSelector.Text = "Лабораторна робота";
			// 
			// l_labWorkDisciplineSelector
			// 
			this.l_labWorkDisciplineSelector.AutoSize = true;
			this.l_labWorkDisciplineSelector.Location = new System.Drawing.Point(131, 71);
			this.l_labWorkDisciplineSelector.Name = "l_labWorkDisciplineSelector";
			this.l_labWorkDisciplineSelector.Size = new System.Drawing.Size(52, 13);
			this.l_labWorkDisciplineSelector.TabIndex = 14;
			this.l_labWorkDisciplineSelector.Text = "Предмет";
			// 
			// cb_labWorkDisciplineSelector
			// 
			this.cb_labWorkDisciplineSelector.FormattingEnabled = true;
			this.cb_labWorkDisciplineSelector.Location = new System.Drawing.Point(189, 68);
			this.cb_labWorkDisciplineSelector.Name = "cb_labWorkDisciplineSelector";
			this.cb_labWorkDisciplineSelector.Size = new System.Drawing.Size(190, 21);
			this.cb_labWorkDisciplineSelector.TabIndex = 13;
			this.cb_labWorkDisciplineSelector.SelectedIndexChanged += new System.EventHandler(this.cb_labWorkDisciplineSelector_SelectedIndexChanged);
			// 
			// cb_labWorkSelector
			// 
			this.cb_labWorkSelector.FormattingEnabled = true;
			this.cb_labWorkSelector.Location = new System.Drawing.Point(189, 102);
			this.cb_labWorkSelector.Name = "cb_labWorkSelector";
			this.cb_labWorkSelector.Size = new System.Drawing.Size(190, 21);
			this.cb_labWorkSelector.TabIndex = 12;
			// 
			// b_editLabWork
			// 
			this.b_editLabWork.Location = new System.Drawing.Point(74, 147);
			this.b_editLabWork.Name = "b_editLabWork";
			this.b_editLabWork.Size = new System.Drawing.Size(75, 23);
			this.b_editLabWork.TabIndex = 10;
			this.b_editLabWork.Text = "Змінити";
			this.b_editLabWork.UseVisualStyleBackColor = true;
			this.b_editLabWork.Click += new System.EventHandler(this.b_editLabWork_Click);
			// 
			// b_createLabWork
			// 
			this.b_createLabWork.Location = new System.Drawing.Point(207, 23);
			this.b_createLabWork.Name = "b_createLabWork";
			this.b_createLabWork.Size = new System.Drawing.Size(75, 23);
			this.b_createLabWork.TabIndex = 9;
			this.b_createLabWork.Text = "Створити";
			this.b_createLabWork.UseVisualStyleBackColor = true;
			this.b_createLabWork.Click += new System.EventHandler(this.b_createLabWork_Click);
			// 
			// page_question
			// 
			this.page_question.Controls.Add(this.b_deleteQuestion);
			this.page_question.Controls.Add(this.cb_questionSelector);
			this.page_question.Controls.Add(this.cb_questionLabWorkSelector);
			this.page_question.Controls.Add(this.cb_questionDisciplineSelector);
			this.page_question.Controls.Add(this.l_questionSelector);
			this.page_question.Controls.Add(this.l_questionLabWorkSelector);
			this.page_question.Controls.Add(this.l_questionDisciplineSelector);
			this.page_question.Controls.Add(this.b_editQuestion);
			this.page_question.Controls.Add(this.b_createQuestion);
			this.page_question.Location = new System.Drawing.Point(4, 22);
			this.page_question.Name = "page_question";
			this.page_question.Padding = new System.Windows.Forms.Padding(3);
			this.page_question.Size = new System.Drawing.Size(460, 241);
			this.page_question.TabIndex = 1;
			this.page_question.Text = "Запитання";
			this.page_question.UseVisualStyleBackColor = true;
			this.page_question.Enter += new System.EventHandler(this.page_question_Enter);
			// 
			// b_deleteQuestion
			// 
			this.b_deleteQuestion.Location = new System.Drawing.Point(313, 159);
			this.b_deleteQuestion.Name = "b_deleteQuestion";
			this.b_deleteQuestion.Size = new System.Drawing.Size(75, 23);
			this.b_deleteQuestion.TabIndex = 31;
			this.b_deleteQuestion.Text = "Видалити";
			this.b_deleteQuestion.UseVisualStyleBackColor = true;
			this.b_deleteQuestion.Click += new System.EventHandler(this.b_deleteQuestion_Click);
			// 
			// cb_questionSelector
			// 
			this.cb_questionSelector.FormattingEnabled = true;
			this.cb_questionSelector.Location = new System.Drawing.Point(164, 115);
			this.cb_questionSelector.Name = "cb_questionSelector";
			this.cb_questionSelector.Size = new System.Drawing.Size(224, 21);
			this.cb_questionSelector.TabIndex = 30;
			// 
			// cb_questionLabWorkSelector
			// 
			this.cb_questionLabWorkSelector.FormattingEnabled = true;
			this.cb_questionLabWorkSelector.Location = new System.Drawing.Point(164, 85);
			this.cb_questionLabWorkSelector.Name = "cb_questionLabWorkSelector";
			this.cb_questionLabWorkSelector.Size = new System.Drawing.Size(224, 21);
			this.cb_questionLabWorkSelector.TabIndex = 29;
			this.cb_questionLabWorkSelector.SelectedIndexChanged += new System.EventHandler(this.cb_questionLabWorkSelector_SelectedIndexChanged);
			// 
			// cb_questionDisciplineSelector
			// 
			this.cb_questionDisciplineSelector.FormattingEnabled = true;
			this.cb_questionDisciplineSelector.Location = new System.Drawing.Point(164, 56);
			this.cb_questionDisciplineSelector.Name = "cb_questionDisciplineSelector";
			this.cb_questionDisciplineSelector.Size = new System.Drawing.Size(224, 21);
			this.cb_questionDisciplineSelector.TabIndex = 28;
			this.cb_questionDisciplineSelector.SelectedIndexChanged += new System.EventHandler(this.cb_questionDisciplineSelector_SelectedIndexChanged);
			// 
			// l_questionSelector
			// 
			this.l_questionSelector.AutoSize = true;
			this.l_questionSelector.Location = new System.Drawing.Point(82, 118);
			this.l_questionSelector.Name = "l_questionSelector";
			this.l_questionSelector.Size = new System.Drawing.Size(61, 13);
			this.l_questionSelector.TabIndex = 27;
			this.l_questionSelector.Text = "Запитання";
			// 
			// l_questionLabWorkSelector
			// 
			this.l_questionLabWorkSelector.AutoSize = true;
			this.l_questionLabWorkSelector.Location = new System.Drawing.Point(31, 88);
			this.l_questionLabWorkSelector.Name = "l_questionLabWorkSelector";
			this.l_questionLabWorkSelector.Size = new System.Drawing.Size(112, 13);
			this.l_questionLabWorkSelector.TabIndex = 26;
			this.l_questionLabWorkSelector.Text = "Лабораторна робота";
			// 
			// l_questionDisciplineSelector
			// 
			this.l_questionDisciplineSelector.AutoSize = true;
			this.l_questionDisciplineSelector.Location = new System.Drawing.Point(91, 59);
			this.l_questionDisciplineSelector.Name = "l_questionDisciplineSelector";
			this.l_questionDisciplineSelector.Size = new System.Drawing.Size(52, 13);
			this.l_questionDisciplineSelector.TabIndex = 25;
			this.l_questionDisciplineSelector.Text = "Предмет";
			// 
			// b_editQuestion
			// 
			this.b_editQuestion.Location = new System.Drawing.Point(34, 159);
			this.b_editQuestion.Name = "b_editQuestion";
			this.b_editQuestion.Size = new System.Drawing.Size(75, 23);
			this.b_editQuestion.TabIndex = 23;
			this.b_editQuestion.Text = "Змінити";
			this.b_editQuestion.UseVisualStyleBackColor = true;
			this.b_editQuestion.Click += new System.EventHandler(this.b_editQuestion_Click);
			// 
			// b_createQuestion
			// 
			this.b_createQuestion.Location = new System.Drawing.Point(204, 6);
			this.b_createQuestion.Name = "b_createQuestion";
			this.b_createQuestion.Size = new System.Drawing.Size(75, 23);
			this.b_createQuestion.TabIndex = 22;
			this.b_createQuestion.Text = "Створити";
			this.b_createQuestion.UseVisualStyleBackColor = true;
			this.b_createQuestion.Click += new System.EventHandler(this.b_createQuestion_Click);
			// 
			// page_results
			// 
			this.page_results.Controls.Add(this.b_downloadCsv);
			this.page_results.Controls.Add(this.gb_resultFilters);
			this.page_results.Controls.Add(this.b_downloadJson);
			this.page_results.Controls.Add(this.b_downloadXml);
			this.page_results.Controls.Add(this.b_quickResult);
			this.page_results.Location = new System.Drawing.Point(4, 22);
			this.page_results.Name = "page_results";
			this.page_results.Size = new System.Drawing.Size(460, 241);
			this.page_results.TabIndex = 4;
			this.page_results.Text = "Результати";
			this.page_results.UseVisualStyleBackColor = true;
			this.page_results.Enter += new System.EventHandler(this.page_results_Enter);
			// 
			// b_downloadCsv
			// 
			this.b_downloadCsv.Location = new System.Drawing.Point(363, 165);
			this.b_downloadCsv.Name = "b_downloadCsv";
			this.b_downloadCsv.Size = new System.Drawing.Size(75, 45);
			this.b_downloadCsv.TabIndex = 5;
			this.b_downloadCsv.Text = "Експорт в csv";
			this.b_downloadCsv.UseVisualStyleBackColor = true;
			this.b_downloadCsv.Click += new System.EventHandler(this.b_downloadCsv_Click);
			// 
			// gb_resultFilters
			// 
			this.gb_resultFilters.Controls.Add(this.cb_descSort);
			this.gb_resultFilters.Controls.Add(this.label5);
			this.gb_resultFilters.Controls.Add(this.label4);
			this.gb_resultFilters.Controls.Add(this.label3);
			this.gb_resultFilters.Controls.Add(this.label2);
			this.gb_resultFilters.Controls.Add(this.label1);
			this.gb_resultFilters.Controls.Add(this.cb_filterLabWork);
			this.gb_resultFilters.Controls.Add(this.cb_filterDiscipline);
			this.gb_resultFilters.Controls.Add(this.dtp_filterEndDate);
			this.gb_resultFilters.Controls.Add(this.dtp_filterStartDate);
			this.gb_resultFilters.Controls.Add(this.cb_filterStudent);
			this.gb_resultFilters.Location = new System.Drawing.Point(8, 12);
			this.gb_resultFilters.Name = "gb_resultFilters";
			this.gb_resultFilters.Size = new System.Drawing.Size(337, 193);
			this.gb_resultFilters.TabIndex = 4;
			this.gb_resultFilters.TabStop = false;
			this.gb_resultFilters.Text = "Фільтри";
			// 
			// cb_descSort
			// 
			this.cb_descSort.AutoSize = true;
			this.cb_descSort.Checked = true;
			this.cb_descSort.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cb_descSort.Location = new System.Drawing.Point(103, 170);
			this.cb_descSort.Name = "cb_descSort";
			this.cb_descSort.Size = new System.Drawing.Size(144, 17);
			this.cb_descSort.TabIndex = 11;
			this.cb_descSort.Text = "від новіших до старіших";
			this.cb_descSort.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(51, 147);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(48, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Дата по";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(55, 121);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(42, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "Дата з";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(25, 89);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(74, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Лабораторна";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(45, 60);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Предмет";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(50, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(47, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Студент";
			// 
			// cb_filterLabWork
			// 
			this.cb_filterLabWork.FormattingEnabled = true;
			this.cb_filterLabWork.Location = new System.Drawing.Point(103, 84);
			this.cb_filterLabWork.Name = "cb_filterLabWork";
			this.cb_filterLabWork.Size = new System.Drawing.Size(200, 21);
			this.cb_filterLabWork.TabIndex = 4;
			// 
			// cb_filterDiscipline
			// 
			this.cb_filterDiscipline.FormattingEnabled = true;
			this.cb_filterDiscipline.Location = new System.Drawing.Point(103, 57);
			this.cb_filterDiscipline.Name = "cb_filterDiscipline";
			this.cb_filterDiscipline.Size = new System.Drawing.Size(200, 21);
			this.cb_filterDiscipline.TabIndex = 3;
			this.cb_filterDiscipline.TextChanged += new System.EventHandler(this.cb_filterDiscipline_TextChanged);
			// 
			// dtp_filterEndDate
			// 
			this.dtp_filterEndDate.CustomFormat = "dd.MM.yyyy";
			this.dtp_filterEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtp_filterEndDate.Location = new System.Drawing.Point(103, 141);
			this.dtp_filterEndDate.Name = "dtp_filterEndDate";
			this.dtp_filterEndDate.Size = new System.Drawing.Size(200, 20);
			this.dtp_filterEndDate.TabIndex = 2;
			// 
			// dtp_filterStartDate
			// 
			this.dtp_filterStartDate.CustomFormat = "dd.MM.yyyy";
			this.dtp_filterStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtp_filterStartDate.Location = new System.Drawing.Point(103, 115);
			this.dtp_filterStartDate.Name = "dtp_filterStartDate";
			this.dtp_filterStartDate.Size = new System.Drawing.Size(200, 20);
			this.dtp_filterStartDate.TabIndex = 1;
			// 
			// cb_filterStudent
			// 
			this.cb_filterStudent.FormattingEnabled = true;
			this.cb_filterStudent.Location = new System.Drawing.Point(103, 29);
			this.cb_filterStudent.Name = "cb_filterStudent";
			this.cb_filterStudent.Size = new System.Drawing.Size(200, 21);
			this.cb_filterStudent.TabIndex = 0;
			// 
			// b_downloadJson
			// 
			this.b_downloadJson.Location = new System.Drawing.Point(363, 114);
			this.b_downloadJson.Name = "b_downloadJson";
			this.b_downloadJson.Size = new System.Drawing.Size(75, 45);
			this.b_downloadJson.TabIndex = 3;
			this.b_downloadJson.Text = "Експорт в json";
			this.b_downloadJson.UseVisualStyleBackColor = true;
			this.b_downloadJson.Click += new System.EventHandler(this.b_downloadJson_Click);
			// 
			// b_downloadXml
			// 
			this.b_downloadXml.Location = new System.Drawing.Point(363, 63);
			this.b_downloadXml.Name = "b_downloadXml";
			this.b_downloadXml.Size = new System.Drawing.Size(75, 45);
			this.b_downloadXml.TabIndex = 2;
			this.b_downloadXml.Text = "Експорт в xml";
			this.b_downloadXml.UseVisualStyleBackColor = true;
			this.b_downloadXml.Click += new System.EventHandler(this.b_downloadXml_Click);
			// 
			// b_quickResult
			// 
			this.b_quickResult.Location = new System.Drawing.Point(363, 12);
			this.b_quickResult.Name = "b_quickResult";
			this.b_quickResult.Size = new System.Drawing.Size(75, 45);
			this.b_quickResult.TabIndex = 0;
			this.b_quickResult.Text = "Швидкий результат";
			this.b_quickResult.UseVisualStyleBackColor = true;
			this.b_quickResult.Click += new System.EventHandler(this.b_quickResult_Click);
			// 
			// page_import
			// 
			this.page_import.Controls.Add(this.b_importXml);
			this.page_import.Controls.Add(this.b_importJson);
			this.page_import.Location = new System.Drawing.Point(4, 22);
			this.page_import.Name = "page_import";
			this.page_import.Size = new System.Drawing.Size(460, 241);
			this.page_import.TabIndex = 5;
			this.page_import.Text = "Імпорт";
			this.page_import.UseVisualStyleBackColor = true;
			// 
			// b_importXml
			// 
			this.b_importXml.Location = new System.Drawing.Point(190, 132);
			this.b_importXml.Name = "b_importXml";
			this.b_importXml.Size = new System.Drawing.Size(80, 39);
			this.b_importXml.TabIndex = 1;
			this.b_importXml.Text = "Імпорт з xml";
			this.b_importXml.UseVisualStyleBackColor = true;
			this.b_importXml.Click += new System.EventHandler(this.b_importXml_Click);
			// 
			// b_importJson
			// 
			this.b_importJson.Location = new System.Drawing.Point(190, 70);
			this.b_importJson.Name = "b_importJson";
			this.b_importJson.Size = new System.Drawing.Size(80, 39);
			this.b_importJson.TabIndex = 0;
			this.b_importJson.Text = "Імпорт з json";
			this.b_importJson.UseVisualStyleBackColor = true;
			this.b_importJson.Click += new System.EventHandler(this.b_importJson_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(59, 80);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(52, 13);
			this.label6.TabIndex = 15;
			this.label6.Text = "Предмет";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(79, 103);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(47, 13);
			this.label7.TabIndex = 13;
			this.label7.Text = "Студент";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(468, 267);
			this.Controls.Add(this.tabControl1);
			this.Name = "MainForm";
			this.Text = "Адмін-панель";
			this.tabControl1.ResumeLayout(false);
			this.page_user.ResumeLayout(false);
			this.page_user.PerformLayout();
			this.page_discipline.ResumeLayout(false);
			this.page_discipline.PerformLayout();
			this.page_createLab.ResumeLayout(false);
			this.page_createLab.PerformLayout();
			this.page_question.ResumeLayout(false);
			this.page_question.PerformLayout();
			this.page_results.ResumeLayout(false);
			this.gb_resultFilters.ResumeLayout(false);
			this.gb_resultFilters.PerformLayout();
			this.page_import.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage page_user;
		private System.Windows.Forms.TabPage page_question;
		private System.Windows.Forms.TabPage page_createLab;
		private System.Windows.Forms.TabPage page_discipline;
		private System.Windows.Forms.ComboBox cb_userSelector;
		private System.Windows.Forms.Button b_editUser;
		private System.Windows.Forms.Button b_createUser;
		private System.Windows.Forms.ComboBox cb_disciplineSelector;
		private System.Windows.Forms.Button b_editDiscipline;
		private System.Windows.Forms.Button b_createDiscipline;
		private System.Windows.Forms.ComboBox cb_labWorkSelector;
		private System.Windows.Forms.Button b_editLabWork;
		private System.Windows.Forms.Button b_createLabWork;
		private System.Windows.Forms.ComboBox cb_labWorkDisciplineSelector;
		private System.Windows.Forms.Button b_editQuestion;
		private System.Windows.Forms.Button b_createQuestion;
		private System.Windows.Forms.Label l_labWorkSelector;
		private System.Windows.Forms.Label l_labWorkDisciplineSelector;
		private System.Windows.Forms.ComboBox cb_questionSelector;
		private System.Windows.Forms.ComboBox cb_questionLabWorkSelector;
		private System.Windows.Forms.ComboBox cb_questionDisciplineSelector;
		private System.Windows.Forms.Label l_questionSelector;
		private System.Windows.Forms.Label l_questionLabWorkSelector;
		private System.Windows.Forms.Label l_questionDisciplineSelector;
		private System.Windows.Forms.TabPage page_results;
		private System.Windows.Forms.Button b_downloadXml;
		private System.Windows.Forms.Button b_deleteUser;
		private System.Windows.Forms.Button b_deleteDiscipline;
		private System.Windows.Forms.Button b_deleteLabWork;
		private System.Windows.Forms.Button b_deleteQuestion;
		private System.Windows.Forms.Button b_downloadJson;
		private System.Windows.Forms.GroupBox gb_resultFilters;
		private System.Windows.Forms.ComboBox cb_filterLabWork;
		private System.Windows.Forms.ComboBox cb_filterDiscipline;
		private System.Windows.Forms.DateTimePicker dtp_filterEndDate;
		private System.Windows.Forms.DateTimePicker dtp_filterStartDate;
		private System.Windows.Forms.ComboBox cb_filterStudent;
		private System.Windows.Forms.Button b_quickResult;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button b_downloadCsv;
		private System.Windows.Forms.CheckBox cb_descSort;
		private System.Windows.Forms.TabPage page_import;
		private System.Windows.Forms.Button b_importJson;
		private System.Windows.Forms.Button b_importXml;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
	}
}