﻿namespace Diplom
{
	partial class AdminLoginForm
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent() {
			this.SuspendLayout();
			// 
			// tbLogin
			// 
			this.tbLogin.Location = new System.Drawing.Point(102, 41);
			this.tbLogin.Size = new System.Drawing.Size(142, 20);
			// 
			// tbPassword
			// 
			this.tbPassword.Location = new System.Drawing.Point(102, 87);
			this.tbPassword.Size = new System.Drawing.Size(142, 20);
			// 
			// bLogin
			// 
			this.bLogin.Location = new System.Drawing.Point(22, 152);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(53, 44);
			this.label1.Size = new System.Drawing.Size(34, 13);
			this.label1.Text = "Логін";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(53, 90);
			// 
			// b_register
			// 
			this.b_register.Location = new System.Drawing.Point(135, 152);
			// 
			// AdminLoginForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(313, 230);
			this.Name = "AdminLoginForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
	}
}

