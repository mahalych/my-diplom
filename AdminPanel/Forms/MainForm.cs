﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using CsvHelper;
using Diplom.EntityForms;
using DiplomHelper;
using DiplomHelper.Db;
using DiplomHelper.Entities;
using DiplomHelper.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Diplom
{
	public partial class MainForm : BaseForm
	{
		#region Fields: private

		private static byte[] _key;
		private StudentEntityForm _studentEntityForm;
		private DisciplineEntityForm _disciplineEntityForm;
		private LabWorkEntityForm _labWorkEntityForm;
		private QuestionEntityForm _questionEntityForm;
		private QuickResultsForm _quickResultsForm;

		#endregion

		#region Constructors: public

		public MainForm() => InitializeComponent();

		public MainForm(string connectionString) : base(connectionString) {
			InitializeComponent();
			_key = SHA256.Create().ComputeHash(Convert.FromBase64String(ConfigurationManager.AppSettings["additionalInfo"]));
			_studentEntityForm = new StudentEntityForm(this, connectionString);
			_disciplineEntityForm = new DisciplineEntityForm(this, connectionString);
			_labWorkEntityForm = new LabWorkEntityForm(this, connectionString);
			_questionEntityForm = new QuestionEntityForm(this, connectionString, (byte[])_key.Clone());
			_quickResultsForm = new QuickResultsForm(this, connectionString, (byte[])_key.Clone());
		}

		#endregion

		#region User

		private void b_deleteUser_Click(object sender, EventArgs e) {
			var student = cb_userSelector.SelectedItem as Student;
			if (student is null) {
				MessageBox.Show("Оберіть студента, якого хочете видалити", "Оберіть студента",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string studentId = student.Id.ToString();
			string sqlText = $"SELECT TOP 1 1 FROM [SysStudentDiscipline] WHERE [StudentId] = '{studentId}' UNION " +
				$"SELECT TOP 1 1 FROM [SysStudentLabWorkGrades] WHERE [StudentId] = '{studentId}'";
			var customQuery = new CustomQuery(_connectionString, sqlText);
			bool isNotExistsDependedRecords = customQuery.ExecuteScalar<int>() != 1;
			if (isNotExistsDependedRecords) {
				var delete =
					new Delete(_connectionString, "Student")
					.Where("Id").IsEqual(studentId) as Delete;
				delete.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			DialogResult dialogResult = MessageBox.Show("Інсують залежні записи. Вони будуть видалені також. Продовжити?",
				"Існують залежні записи", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dialogResult == DialogResult.Yes) {
				var delete1 =
					new Delete(_connectionString, "SysStudentDiscipline")
					.Where("StudentId").IsEqual(studentId) as Delete;
				delete1.Execute();
				var delete2 =
					new Delete(_connectionString, "SysStudentLabWorkGrades")
					.Where("StudentId").IsEqual(studentId) as Delete;
				delete2.Execute();
				var delete3 =
					new Delete(_connectionString, "Student")
					.Where("Id").IsEqual(studentId) as Delete;
				delete3.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void b_createUser_Click(object sender, EventArgs e) {
			_studentEntityForm.SetData(State.Add);
			_studentEntityForm.Show();
		}

		private void b_editUser_Click(object sender, EventArgs e) {
			var student = cb_userSelector.SelectedItem as Student;
			if (student is null) {
				MessageBox.Show("Оберіть студента, якого хочете відредагувати",
					"Оберіть студента", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			_studentEntityForm.SetData(State.Edit, student);
			_studentEntityForm.Show();
		}

		private void page_user_Enter(object sender, EventArgs e) =>
			Helper.LoadStudentsToComboBox(_connectionString, cb_userSelector);

		#endregion

		#region question

		private void page_question_Enter(object sender, EventArgs e) {
			Helper.LoadDisciplinesToComboBox(_connectionString, cb_questionDisciplineSelector);
		}

		private void b_createQuestion_Click(object sender, EventArgs e) {
			_questionEntityForm.SetData(State.Add);
			_questionEntityForm.Show();
		}

		private void b_editQuestion_Click(object sender, EventArgs e) {
			var q = cb_questionSelector.SelectedItem as Question;
			if (q is null) {
				MessageBox.Show("Оберіть запитання, якого хочете відредагувати", "Оберіть запитання",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			_questionEntityForm.SetData(State.Edit, q);
			_questionEntityForm.Show();
		}

		private void cb_questionDisciplineSelector_SelectedIndexChanged(object sender, EventArgs e) {
			cb_questionLabWorkSelector.SelectedItem =
				cb_questionSelector.SelectedItem = null;
			cb_questionLabWorkSelector.Items.Clear();
			cb_questionSelector.Items.Clear();
			var d = (sender as ComboBox)?.SelectedItem as Discipline;
			if (d is null) {
				return;
			}
			Helper.LoadDisciplinesLabWorksToComboBox(_connectionString, cb_questionLabWorkSelector, d.Id);
		}

		private void cb_questionLabWorkSelector_SelectedIndexChanged(object sender, EventArgs e) {
			var lw = (sender as ComboBox)?.SelectedItem as LaboratoryWork;
			if (lw is null) {
				return;
			}
			cb_questionSelector.SelectedItem = null;
			cb_questionSelector.Items.Clear();
			Question[] questions = Helper.GetQuestions(_connectionString, lw.Id);
			cb_questionSelector.Items.AddRange(questions);
		}

		private void b_deleteQuestion_Click(object sender, EventArgs e) {
			var question = cb_questionSelector.SelectedItem as Question;
			if (question is null) {
				MessageBox.Show("Оберіть запитання, якого хочете видалити", "Оберіть запитання",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string questionId = question.Id.ToString();
			string sqlText = $"SELECT TOP 1 1 FROM [AnswerVariant] WHERE [QuestionId] = '{questionId}'";
			var customQuery = new CustomQuery(_connectionString, sqlText);
			bool isNotExistsDependedRecords = customQuery.ExecuteScalar<int>() != 1;
			if (isNotExistsDependedRecords) {
				var delete =
					new Delete(_connectionString, "Question")
					.Where("Id").IsEqual(questionId) as Delete;
				delete.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			DialogResult dialogResult = MessageBox.Show("Інсують залежні записи. Вони будуть видалені також. Продовжити?",
				"Існують залежні записи", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dialogResult == DialogResult.Yes) {
				var delete1 =
					new Delete(_connectionString, "AnswerVariant")
					.Where("QuestionId").IsEqual(questionId) as Delete;
				delete1.Execute();
				var delete2 =
					new Delete(_connectionString, "Question")
					.Where("Id").IsEqual(questionId) as Delete;
				delete2.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		#endregion

		#region lab work

		private void page_lab_Enter(object sender, EventArgs e) {
			LabWorkTabLoadDisciplines(cb_labWorkDisciplineSelector);
		}

		private void LabWorkTabLoadDisciplines(ComboBox target) {
			target.SelectedItem = null;
			Discipline[] disciplines = Helper.GetDisciplineList(_connectionString);
			target.Items.Clear();
			target.Items.AddRange(disciplines);
		}

		private void b_createLabWork_Click(object sender, EventArgs e) {
			_labWorkEntityForm.SetData(State.Add);
			_labWorkEntityForm.Show();
		}

		private void b_editLabWork_Click(object sender, EventArgs e) {
			var labWork = cb_labWorkSelector.SelectedItem as LaboratoryWork;
			if (labWork is null) {
				MessageBox.Show("Оберіть лабораторну роботу, яку хочете відредагувати", "Оберіть лабораторну роботу",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			_labWorkEntityForm.SetData(State.Edit, labWork);
			_labWorkEntityForm.Show();
		}


		private void cb_labWorkDisciplineSelector_SelectedIndexChanged(object sender, EventArgs e) {
			cb_labWorkSelector.SelectedItem = null;
			var currentDiscipline = cb_labWorkDisciplineSelector.SelectedItem as Discipline;
			if (currentDiscipline is null) {
				cb_labWorkSelector.Items.Clear();
				return;
			}
			Helper.LoadDisciplinesLabWorksToComboBox(_connectionString, cb_labWorkSelector, currentDiscipline.Id);
		}

		private void b_deleteLabWork_Click(object sender, EventArgs e) {
			var labWork = cb_labWorkSelector.SelectedItem as LaboratoryWork;
			if (labWork is null) {
				MessageBox.Show("Оберіть лабораторну роботу, яку хочете видалити", "Оберіть лабораторну роботу",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string labWorkId = labWork.Id.ToString();
			string sqlText = "SELECT TOP 1 1 FROM [SysStudentLabWorkGrades] " +
				$"WHERE [LaboratoryWorkId] = '{labWorkId}' UNION " +
				$"SELECT TOP 1 1 FROM [Question] WHERE [LaboratoryWorkId] = '{labWorkId}'";
			var customQuery = new CustomQuery(_connectionString, sqlText);
			bool isNotExistsDependedRecords = customQuery.ExecuteScalar<int>() != 1;
			if (isNotExistsDependedRecords) {
				var delete =
					new Delete(_connectionString, "LaboratoryWork")
					.Where("Id").IsEqual(labWorkId) as Delete;
				delete.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			DialogResult dialogResult = MessageBox.Show("Інсують залежні записи. Вони будуть видалені також. Продовжити?",
				"Існують залежні записи", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dialogResult == DialogResult.Yes) {
				var delete1 =
					new Delete(_connectionString, "SysStudentLabWorkGrades")
					.Where("LaboratoryWorkId").IsEqual(labWorkId) as Delete;
				delete1.Execute();
				var delete =
					new Delete(_connectionString, "AnswerVariant")
					.Where().Exists(new Select(_connectionString)
							.Column<Guid>("Id")
						.From("Question")
						.Where("Question", "Id").IsEqual("AnswerVariant", "QuestionId")
							.And("LaboratoryWorkId").IsEqual(labWorkId) as Select
					) as Delete;
				delete.Execute();
				var delete2 =
					new Delete(_connectionString, "Question")
					.Where("LaboratoryWorkId").IsEqual(labWorkId) as Delete;
				delete2.Execute();
				var delete3 =
					new Delete(_connectionString, "LaboratoryWork")
					.Where("Id").IsEqual(labWorkId) as Delete;
				delete3.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			cb_labWorkDisciplineSelector.SelectedItem =
				cb_labWorkSelector.SelectedItem = null;
		}

		#endregion

		#region discipline

		private void b_createDiscipline_Click(object sender, EventArgs e) {
			_disciplineEntityForm.SetData(State.Add);
			_disciplineEntityForm.Show();
		}

		private void b_editDiscipline_Click(object sender, EventArgs e) {
			var discipline = cb_disciplineSelector.SelectedItem as Discipline;
			if (discipline is null) {
				MessageBox.Show("Оберіть предмет, який хочете відредагувати", "Оберіть предмет",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			_disciplineEntityForm.SetData(State.Edit, discipline);
			_disciplineEntityForm.Show();
		}

		private void page_discipline_Enter(object sender, EventArgs e) {
			Helper.LoadDisciplinesToComboBox(_connectionString, cb_disciplineSelector);
		}

		private void b_deleteDiscipline_Click(object sender, EventArgs e) {
			var discipline = cb_disciplineSelector.SelectedItem as Discipline;
			if (discipline is null) {
				MessageBox.Show("Оберіть предмет, яку хочете видалити", "Оберіть предмет",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string disciplineId = discipline.Id.ToString();
			string sqlText = $"SELECT TOP 1 1 FROM [SysStudentDiscipline] WHERE [DisciplineId] = '{disciplineId}' UNION " +
				$"SELECT TOP 1 1 FROM [LaboratoryWork] WHERE [DisciplineId] = '{disciplineId}'";
			var customQuery = new CustomQuery(_connectionString, sqlText);
			bool isNotExistsDependedRecords = customQuery.ExecuteScalar<int>() != 1;
			if (isNotExistsDependedRecords) {
				var delete =
					new Delete(_connectionString, "Discipline")
					.Where("Id").IsEqual(disciplineId) as Delete;
				delete.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			DialogResult dialogResult = MessageBox.Show("Інсують залежні записи. Вони будуть видалені також. Продовжити?",
				"Існують залежні записи", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dialogResult == DialogResult.Yes) {
				var delete1 =
					new Delete(_connectionString, "SysStudentDiscipline")
					.Where("DisciplineId").IsEqual(disciplineId) as Delete;
				delete1.Execute();
				var delete2 =
					new Delete(_connectionString, "SysStudentLabWorkGrades")
					.Where().Exists(new Select(_connectionString)
							.Column<Guid>("Id")
						.From("LaboratoryWork")
						.Where("LaboratoryWork", "Id").IsEqual("SysStudentLabWorkGrades", "LaboratoryWorkId")
							.And("DisciplineId").IsEqual(disciplineId) as Select
					) as Delete;
				delete2.Execute();
				var delete3 =
					new Delete(_connectionString, "AnswerVariant")
					.Where().Exists(new Select(_connectionString)
							.Column<Guid>("Question", "Id")
						.From("Question")
						.Where("AnswerVariant", "QuestionId").IsEqual("Question", "Id")
							.And().Exists(new Select(_connectionString)
								.Column<Guid>("LaboratoryWork", "Id")
							.From("LaboratoryWork")
							.Where("LaboratoryWork", "Id").IsEqual("Question", "LaboratoryWorkId")
								.And("DisciplineId").IsEqual(disciplineId) as Select
						) as Select
					) as Delete;
				delete3.Execute();
				var delete4 =
					new Delete(_connectionString, "Question")
					.Where().Exists(new Select(_connectionString)
							.Column<Guid>("LaboratoryWork", "Id")
						.From("LaboratoryWork")
						.Where("LaboratoryWork", "Id").IsEqual("Question", "LaboratoryWorkId")
							.And("DisciplineId").IsEqual(disciplineId) as Select
					) as Delete;
				delete4.Execute();
				var delete5 =
					new Delete(_connectionString, "LaboratoryWork")
					.Where("DisciplineId").IsEqual(disciplineId) as Delete;
				delete5.Execute();
				var delete6 =
					new Delete(_connectionString, "Discipline")
					.Where("Id").IsEqual(disciplineId) as Delete;
				delete6.Execute();
				MessageBox.Show("Видалення успішне", "Успіх",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			cb_disciplineSelector.SelectedItem = null;
		}

		#endregion

		#region Results & Export

		private async void b_downloadXml_Click(object sender, EventArgs e) {
			string fileName;
			using (var sfd = new SaveFileDialog() {
				Filter = "*.xml|*.xml"
			}) {
				if (sfd.ShowDialog() != DialogResult.OK) {
					return;
				}
				fileName = sfd.FileName;
			}
			Task<TestResult[]> results = Helper.GetTestResults(_connectionString, GetTestResultFilter(),
				(byte[])_key.Clone());
			var resultNode = new XElement("Results");
			foreach (TestResult result in await results) {
				resultNode.Add(new XElement("Result",
					new XElement("DisciplineName", result.DisciplineName),
					new XElement("LabWorkNumber", result.LabWorkNumber.ToString()),
					new XElement("LabWorkName", result.LabWorkName),
					new XElement("StudCardNumber", result.StudCardNumber),
					new XElement("StudentName", result.StudentName),
					new XElement("ExamDate", result.ExamDate.ToString("dd.MM.yyyy HH:mm:ss")),
					new XElement("Grade", result.Grade.ToString())
				));
			}
			var doc = new XDocument(
				new XDeclaration("1.0", "UTF-8", null),
				resultNode
			);
			try {
				await Helper.ShowMask(this, () => doc.Save(fileName));
				MessageBox.Show("Експорт успішний", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private async void b_downloadJson_Click(object sender, EventArgs e) {
			string fileName;
			using (var sfd = new SaveFileDialog() {
				Filter = "*.json|*.json"
			}) {
				if (sfd.ShowDialog() != DialogResult.OK) {
					return;
				}
				fileName = sfd.FileName;
			}
			Task<TestResult[]> results = Helper.GetTestResults(_connectionString, GetTestResultFilter(),
				(byte[])_key.Clone());
			var content = JArray.Parse(JsonConvert.SerializeObject((await results).Select(item => new {
				item.DisciplineName,
				item.LabWorkNumber,
				item.LabWorkName,
				item.StudCardNumber,
				item.StudentName,
				ExamDate = item.ExamDate.ToString("dd.MM.yyyy HH:mm:ss"),
				item.Grade
			})));
			try {
				await Helper.ShowMask(this, () => {
					using (var sw = new StreamWriter(fileName, false, Encoding.UTF8)) {
						return sw.WriteAsync(content.ToString(Formatting.Indented));
					}
				});
				MessageBox.Show("Експорт успішний", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void b_quickResult_Click(object sender, EventArgs e) {
			_quickResultsForm.TestResultFilter = GetTestResultFilter();
			_quickResultsForm.Show();
		}

		private TestResultFilter GetTestResultFilter() => new TestResultFilter {
			StudentId = (cb_filterStudent.SelectedItem as Student)?.Id ?? default,
			DisciplineId = (cb_filterDiscipline.SelectedItem as Discipline)?.Id ?? default,
			LabWorkId = (cb_filterLabWork.SelectedItem as LaboratoryWork)?.Id ?? default,
			StartDate = dtp_filterStartDate.Value,
			EndDate = dtp_filterEndDate.Value,
			DescSort = cb_descSort.Checked
		};

		private void page_results_Enter(object sender, EventArgs e) {
			Helper.LoadStudentsToComboBox(_connectionString, cb_filterStudent);
			Helper.LoadDisciplinesToComboBox(_connectionString, cb_filterDiscipline);
		}

		private void cb_filterDiscipline_TextChanged(object sender, EventArgs e) {
			if (cb_filterDiscipline.SelectedItem is Discipline d) {
				Helper.LoadDisciplinesLabWorksToComboBox(_connectionString, cb_filterLabWork, d.Id);
				return;
			}
			if (String.IsNullOrEmpty(cb_filterDiscipline.Text)) {
				cb_filterLabWork.SelectedItem = null;
				cb_filterLabWork.Items.Clear();
			}
		}

		private async void b_downloadCsv_Click(object sender, EventArgs e) {
			string fileName;
			using (var sfd = new SaveFileDialog() {
				Filter = "*.csv|*.csv"
			}) {
				if (sfd.ShowDialog() != DialogResult.OK) {
					return;
				}
				fileName = sfd.FileName;
			}
			Task<TestResult[]> task = Helper.GetTestResults(_connectionString, GetTestResultFilter(),
				(byte[])_key.Clone());
			TestResult[] result = await task;
			try {
				await Helper.ShowMask(this, () => {
					using (var sw = new StreamWriter(fileName, false, Encoding.UTF8)) {
						var config = new CsvHelper.Configuration.Configuration() {
							Encoding = Encoding.UTF8,
							Delimiter = ";"
						};
						using (var csv = new CsvWriter(sw, config)) {
							csv.WriteRecords(result);
						}
					}
				});
				MessageBox.Show("Експорт успішний", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		#endregion

		#region Import

		private async void b_importJson_Click(object sender, EventArgs e) {
			string fileName;
			using (var ofd = new OpenFileDialog {
				Filter = "*.json|*.json"
			}) {
				if (ofd.ShowDialog() != DialogResult.OK) {
					return;
				}
				fileName = ofd.FileName;
			}
			try {
				await Helper.ShowMask(this, () => {
					string json;
					using (var sr = new StreamReader(fileName, Encoding.UTF8)) {
						json = sr.ReadToEnd();
					}
					QuestionImport[] questions = JsonConvert.DeserializeObject<QuestionImport[]>(json);
					ImportQuestions(questions);
				});
				MessageBox.Show("Імпорт успішний", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void ImportQuestions(IEnumerable<QuestionImport> questions) {
			IEnumerable<IGrouping<string, QuestionImport>> grouppedByDiscipline =
			questions.Where(q => !String.IsNullOrEmpty(q.Discipline)).GroupBy(q => q.Discipline.Trim());
			grouppedByDiscipline.ForEach(groupDisc => ImportDisciplineQuestions(groupDisc));
		}

		private void ImportDisciplineQuestions(IGrouping<string, QuestionImport> groupDisc) {
			if (!CheckIsDisciplineExists(groupDisc.Key, out Guid disciplineId)) {
				return;
			}
			IEnumerable<IGrouping<int, QuestionImport>> grouppedByLabWork =
				groupDisc.Where(g => g.LabWork != -1).GroupBy(g => g.LabWork);
			grouppedByLabWork.ForEach(groupLw => ImportLabWorkQuestions(groupLw));
		}

		private void ImportLabWorkQuestions(IGrouping<int, QuestionImport> groupLw) {
			if (!CheckIsLabWorkExists(groupLw.Key, out Guid labWorkId)) {
				return;
			}
			foreach (QuestionImport question in groupLw) {
				if (question.Cost <= 0m) {
					continue;
				}
				string questionText = question.QuestionText.Trim();
				if (String.IsNullOrEmpty(questionText) ||
						CheckIsQuestionExists(labWorkId, questionText) ||
						question.AnswerVariants.Where(aw => !String.IsNullOrEmpty(aw.Text)).All(aw => !aw.IsCorrect) ||
						question.AnswerVariants.Where(aw => aw.IsCorrect).All(aw => String.IsNullOrEmpty(aw.Text))) {
					continue;
				}
				var questId = Guid.NewGuid();
				Helper.InsertQuestion(_connectionString, labWorkId,
					questionText, question.Cost, questId);
				Helper.InsertAnswerVariants(_connectionString, questId,
					question.AnswerVariants, (byte[])_key.Clone());
			}
		}

		private bool CheckIsQuestionExists(Guid labWorkId, string questionText) {
			var select =
				new Select(_connectionString)
					.Column<Guid>("Id")
				.From("Question")
				.Where("LaboratoryWorkId").IsEqual(labWorkId)
					.And(new Upper("Text")).IsEqual(questionText.ToUpper()) as Select;
			return select.ExecuteScalar<Guid>() != default;
		}

		private bool CheckIsLabWorkExists(int number, out Guid labWorkId) {
			var select =
				new Select(_connectionString)
					.Column<Guid>("Id")
				.From("LaboratoryWork")
				.Where("Number").IsEqual(number) as Select;
			labWorkId = select.ExecuteScalar<Guid>();
			return labWorkId != default;
		}

		private bool CheckIsDisciplineExists(string disciplineName, out Guid disciplineId) {
			var select =
				new Select(_connectionString)
					.Column<Guid>("Id")
				.From("Discipline")
				.Where(new Upper("Name")).IsEqual(disciplineName.ToUpper()) as Select;
			disciplineId = select.ExecuteScalar<Guid>();
			return disciplineId != default;
		}

		private async void b_importXml_Click(object sender, EventArgs e) {
			string fileName;
			using (var ofd = new OpenFileDialog {
				Filter = "*.xml|*.xml"
			}) {
				if (ofd.ShowDialog() != DialogResult.OK) {
					return;
				}
				fileName = ofd.FileName;
			}
			try {
				await Helper.ShowMask(this, () => {
					var doc = XDocument.Load(fileName);
					IEnumerable<QuestionImport> questions = doc.Root.Descendants("Question")
						.Select(GetQuestionImportFromXElement).ToArray();
					ImportQuestions(questions);
				});
				MessageBox.Show("Імпорт успішний", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private QuestionImport GetQuestionImportFromXElement(XElement questEl) {
			if (!Int32.TryParse(questEl.Element("LabWork")?.Value, out int labWorkNumber)) {
				labWorkNumber = -1;
			}
			if (!Decimal.TryParse(questEl.Element("Cost")?.Value, out decimal questionCost)) {
				questionCost = 0m;
			}
			return new QuestionImport {
				Discipline = questEl.Element("Discipline")?.Value,
				LabWork = labWorkNumber,
				Cost = questionCost,
				QuestionText = questEl.Element("QuestionText")?.Value,
				AnswerVariants = questEl.Element("AnswerVariants")?.Descendants("Variant")
					.Take(4).Select(variantEl => {
						if (!Boolean.TryParse(variantEl.Element("IsCorrect")?.Value, out bool isCorrect)) {
							isCorrect = false;
						}
						return new AnswerVariantShell {
							Text = variantEl.Element("Text")?.Value,
							IsCorrect = isCorrect
						};
					}).ToArray()
			};
		}

		#endregion
	}
}
