﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DiplomHelper;

namespace Diplom
{
	public partial class QuickResultsForm : Form
	{
		private string _connectionString;
		private byte[] _key;
		private Form _parent;
		private TestResultFilter _testResultFilter;

		public QuickResultsForm(Form parent, string connectionString, byte[] key) {
			InitializeComponent();
			_parent = parent;
			_connectionString = connectionString;
			_key = key;
		}

		public TestResultFilter TestResultFilter {
			set {
				_testResultFilter = value;
			}
		}

		private async void b_refreshResult_Click(object sender, EventArgs e) {
			TestResult[] results = await Helper.GetTestResults(_connectionString,
				_testResultFilter, (byte[])_key.Clone(), 100);
			tb_testResults.Text = String.Join(Environment.NewLine, results.Select(entity => entity.ToString()));
		}

		private void ExportResultsForm_FormClosing(object sender, FormClosingEventArgs e) {
			if (e.CloseReason == CloseReason.UserClosing) {
				e.Cancel = true;
				Hide();
			}
		}

		protected override void OnVisibleChanged(EventArgs e) {
			base.OnVisibleChanged(e);
			if (Visible) {
				b_refreshResult_Click(null, null);
				_parent?.Hide();
			} else {
				_parent?.Show();
			}
		}
	}
}
