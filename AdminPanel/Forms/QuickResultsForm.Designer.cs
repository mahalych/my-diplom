﻿namespace Diplom
{
	partial class QuickResultsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.tb_testResults = new System.Windows.Forms.TextBox();
			this.b_refreshResult = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tb_testResults
			// 
			this.tb_testResults.Enabled = false;
			this.tb_testResults.Location = new System.Drawing.Point(12, 12);
			this.tb_testResults.Multiline = true;
			this.tb_testResults.Name = "tb_testResults";
			this.tb_testResults.Size = new System.Drawing.Size(599, 426);
			this.tb_testResults.TabIndex = 6;
			// 
			// b_refreshResult
			// 
			this.b_refreshResult.Location = new System.Drawing.Point(630, 10);
			this.b_refreshResult.Name = "b_refreshResult";
			this.b_refreshResult.Size = new System.Drawing.Size(75, 23);
			this.b_refreshResult.TabIndex = 5;
			this.b_refreshResult.Text = "оновити";
			this.b_refreshResult.UseVisualStyleBackColor = true;
			this.b_refreshResult.Click += new System.EventHandler(this.b_refreshResult_Click);
			// 
			// QuickResultsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(718, 450);
			this.Controls.Add(this.tb_testResults);
			this.Controls.Add(this.b_refreshResult);
			this.Name = "QuickResultsForm";
			this.Text = "Результати";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExportResultsForm_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox tb_testResults;
		private System.Windows.Forms.Button b_refreshResult;
	}
}