﻿namespace Diplom.EntityForms
{
	partial class StudentEntityForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.clb_disciplines = new System.Windows.Forms.CheckedListBox();
			this.l_password = new System.Windows.Forms.Label();
			this.l_cardNum = new System.Windows.Forms.Label();
			this.l_name = new System.Windows.Forms.Label();
			this.tb_password = new System.Windows.Forms.TextBox();
			this.tb_cardNum = new System.Windows.Forms.TextBox();
			this.tb_name = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// b_cancel
			// 
			this.b_cancel.Location = new System.Drawing.Point(237, 154);
			// 
			// b_save
			// 
			this.b_save.Location = new System.Drawing.Point(58, 154);
			// 
			// clb_disciplines
			// 
			this.clb_disciplines.CheckOnClick = true;
			this.clb_disciplines.FormattingEnabled = true;
			this.clb_disciplines.Location = new System.Drawing.Point(333, 12);
			this.clb_disciplines.Name = "clb_disciplines";
			this.clb_disciplines.Size = new System.Drawing.Size(150, 199);
			this.clb_disciplines.TabIndex = 19;
			// 
			// l_password
			// 
			this.l_password.AutoSize = true;
			this.l_password.Location = new System.Drawing.Point(123, 93);
			this.l_password.Name = "l_password";
			this.l_password.Size = new System.Drawing.Size(45, 13);
			this.l_password.TabIndex = 17;
			this.l_password.Text = "Пароль";
			// 
			// l_cardNum
			// 
			this.l_cardNum.AutoSize = true;
			this.l_cardNum.Location = new System.Drawing.Point(12, 56);
			this.l_cardNum.Name = "l_cardNum";
			this.l_cardNum.Size = new System.Drawing.Size(156, 13);
			this.l_cardNum.TabIndex = 16;
			this.l_cardNum.Text = "Номер студентського квитка";
			// 
			// l_name
			// 
			this.l_name.AutoSize = true;
			this.l_name.Location = new System.Drawing.Point(142, 19);
			this.l_name.Name = "l_name";
			this.l_name.Size = new System.Drawing.Size(26, 13);
			this.l_name.TabIndex = 15;
			this.l_name.Text = "Ім\'я";
			// 
			// tb_password
			// 
			this.tb_password.Location = new System.Drawing.Point(174, 90);
			this.tb_password.Name = "tb_password";
			this.tb_password.Size = new System.Drawing.Size(153, 20);
			this.tb_password.TabIndex = 14;
			// 
			// tb_cardNum
			// 
			this.tb_cardNum.Location = new System.Drawing.Point(174, 53);
			this.tb_cardNum.Name = "tb_cardNum";
			this.tb_cardNum.Size = new System.Drawing.Size(153, 20);
			this.tb_cardNum.TabIndex = 13;
			// 
			// tb_name
			// 
			this.tb_name.Location = new System.Drawing.Point(174, 16);
			this.tb_name.Name = "tb_name";
			this.tb_name.Size = new System.Drawing.Size(153, 20);
			this.tb_name.TabIndex = 12;
			// 
			// StudentEntityForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(495, 233);
			this.Controls.Add(this.clb_disciplines);
			this.Controls.Add(this.l_password);
			this.Controls.Add(this.l_cardNum);
			this.Controls.Add(this.l_name);
			this.Controls.Add(this.tb_password);
			this.Controls.Add(this.tb_cardNum);
			this.Controls.Add(this.tb_name);
			this.Name = "StudentEntityForm";
			this.Text = "Студент";
			this.Controls.SetChildIndex(this.b_save, 0);
			this.Controls.SetChildIndex(this.b_cancel, 0);
			this.Controls.SetChildIndex(this.tb_name, 0);
			this.Controls.SetChildIndex(this.tb_cardNum, 0);
			this.Controls.SetChildIndex(this.tb_password, 0);
			this.Controls.SetChildIndex(this.l_name, 0);
			this.Controls.SetChildIndex(this.l_cardNum, 0);
			this.Controls.SetChildIndex(this.l_password, 0);
			this.Controls.SetChildIndex(this.clb_disciplines, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.CheckedListBox clb_disciplines;
		private System.Windows.Forms.Label l_password;
		private System.Windows.Forms.Label l_cardNum;
		private System.Windows.Forms.Label l_name;
		private System.Windows.Forms.TextBox tb_password;
		private System.Windows.Forms.TextBox tb_cardNum;
		private System.Windows.Forms.TextBox tb_name;
	}
}