﻿namespace Diplom.EntityForms
{
	partial class BaseEntityForm<T>
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.b_cancel = new System.Windows.Forms.Button();
			this.b_save = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// b_cancel
			// 
			this.b_cancel.Location = new System.Drawing.Point(374, 214);
			this.b_cancel.Name = "b_cancel";
			this.b_cancel.Size = new System.Drawing.Size(75, 23);
			this.b_cancel.TabIndex = 26;
			this.b_cancel.Text = "скасувати";
			this.b_cancel.UseVisualStyleBackColor = true;
			this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
			// 
			// b_save
			// 
			this.b_save.Location = new System.Drawing.Point(207, 214);
			this.b_save.Name = "b_save";
			this.b_save.Size = new System.Drawing.Size(75, 23);
			this.b_save.TabIndex = 25;
			this.b_save.Text = "зберегти";
			this.b_save.UseVisualStyleBackColor = true;
			this.b_save.Click += new System.EventHandler(this.b_save_Click);
			// 
			// BaseEntityForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.b_cancel);
			this.Controls.Add(this.b_save);
			this.Name = "BaseEntityForm";
			this.Text = "BaseEntityForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseEntityForm_FormClosing);
			this.ResumeLayout(false);

		}

		#endregion

		protected System.Windows.Forms.Button b_cancel;
		protected System.Windows.Forms.Button b_save;
	}
}