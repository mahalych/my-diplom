﻿using System;
using System.Windows.Forms;
using DiplomHelper;
using DiplomHelper.Enums;

namespace Diplom.EntityForms
{
	public partial class BaseEntityForm<T> : Form
	{
		protected BaseForm _parent;
		protected State _state;
		protected string _connectionString;
		protected T _entity;

		protected BaseEntityForm() => InitializeComponent();

		public BaseEntityForm(BaseForm parent, string connectionString) : this() {
			_connectionString = connectionString;
			_parent = parent;
		}

		private void BaseEntityForm_FormClosing(object sender, FormClosingEventArgs e) {
			if (e.CloseReason == CloseReason.UserClosing) {
				e.Cancel = true;
				_state = State.None;
				Hide();
			}
		}

		protected override void OnVisibleChanged(EventArgs e) {
			base.OnVisibleChanged(e);
			if (Visible) {
				_parent?.Hide();
			} else {
				_parent?.Show();
			}
		}

		protected virtual void b_save_Click(object sender, EventArgs e) {
		}

		protected virtual void b_cancel_Click(object sender, EventArgs e) {
			Close();
		}

		public virtual void SetData(State state, T entity) {
			_state = state;
			_entity = entity;
		}
	}
}
