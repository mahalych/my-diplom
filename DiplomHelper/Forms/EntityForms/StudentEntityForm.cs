﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using DiplomHelper;
using DiplomHelper.Db;
using DiplomHelper.Entities;
using DiplomHelper.Enums;

namespace Diplom.EntityForms
{
	public partial class StudentEntityForm : BaseEntityForm<Student>
	{
		private static MD5 _md5 = MD5.Create();

		public StudentEntityForm(BaseForm parent, string connectionString) :
			base(parent, connectionString) => InitializeComponent();

		protected override void OnVisibleChanged(EventArgs e) {
			base.OnVisibleChanged(e);
			if (Visible) {
				LoadUserTabDisciplines();
				if (_parent is LoginForm lf && lf.IsAdminForm) {
					l_cardNum.Text = "Логін";
					l_cardNum.Location = new Point(134, 56);
					Text = "Адміністратор";
					clb_disciplines.Visible = false;
				} else {
					l_cardNum.Text = "Номер студентського квитка";
					l_cardNum.Location = new Point(12, 56);
					Text = "Студент";
					clb_disciplines.Visible = true;
				}
			}
		}

		protected virtual void AddStudent(string name, string cardNum, string password) {
			if (String.IsNullOrEmpty(cardNum)) {
				MessageBox.Show($"Уведіть номер студентського квитка",
					"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (String.IsNullOrEmpty(password)) {
				MessageBox.Show($"Уведіть пароль!",
					"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (IsStudentExists(cardNum)) {
				MessageBox.Show($"Студент з номером квитка \"{cardNum}\" уже існує!",
					"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			CheckedListBox.CheckedItemCollection selectedDisciplines = clb_disciplines.CheckedItems;
			AuthObject authObject = Helper.GetAuthObject(cardNum, password);
			var newStudentId = new Guid(_md5.ComputeHash(Encoding.UTF8.GetBytes($"{cardNum};{password}{authObject.Salt}")));
			try {
				InsertStudent(name, cardNum, authObject, newStudentId);
				InsertStudentDisciplines(selectedDisciplines, newStudentId);
				MessageBox.Show("Запис створено",
					"Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message,
					"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		protected virtual bool IsStudentExists(string cardNum) {
			var select =
				new Select(_connectionString).Top(1)
					.Column<Guid>("Id")
				.From("Student")
				.Where("StudentCardNumber").IsEqual(cardNum) as Select;
			Guid studentId = select.ExecuteScalar<Guid>();
			return studentId != default;
		}

		protected override void b_save_Click(object sender, EventArgs e) {
			string name = tb_name.Text.Trim();
			if (String.IsNullOrEmpty(name)) {
				MessageBox.Show($"Уведіть ім'я", "Помилка",
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			string cardNum = tb_cardNum.Text.Trim();
			string password = tb_password.Text.Trim();
			if (_state == State.Add) {
				AddStudent(name, cardNum, password);
			} else if (_state == State.Edit) {
				Guid id = _entity.Id;
				UpdateStudent(id, name);
				UpdateStudentDisciplines(id);
			}
			Close();
		}

		private void UpdateStudentDisciplines(Guid studentId) {
			CheckedListBox.CheckedItemCollection selectedDisciplines = clb_disciplines.CheckedItems;
			IEnumerable<Discipline> disciplines = selectedDisciplines.Cast<Discipline>();
			var delete =
				new Delete(_connectionString, "SysStudentDiscipline")
				.Where("StudentId").IsEqual(studentId) as Delete;
			if (disciplines.Any()) {
				delete.And("DisciplineId").Not().In(disciplines.Select(d => (object)d.Id));
			}
			delete.Execute();
			foreach (Discipline discipline in disciplines) {
				var select =
					new Select(_connectionString)
						.Column<Guid>("Id")
					.From("SysStudentDiscipline")
					.Where("StudentId").IsEqual(studentId)
						.And("DisciplineId").IsEqual(discipline.Id) as Select;
				if (select.ExecuteScalar<Guid>() == Guid.Empty) {
					var insert =
						new Insert(_connectionString).Into("SysStudentDiscipline")
							.Set("StudentId", studentId)
							.Set("DisciplineId", discipline.Id) as Insert;
					insert.Execute();
				}
			}
		}

		private void UpdateStudent(Guid id, string name) {
			var update =
				new Update(_connectionString, "Student")
					.Set("Name", name)
				.Where("Id").IsEqual(id) as Update;
			update.Execute();
		}

		private void InsertStudentDisciplines(CheckedListBox.CheckedItemCollection selectedDisciplines,
				Guid newStudentId) {
			foreach (object selectedDiscipline in selectedDisciplines) {
				var discipline = selectedDiscipline as Discipline;
				if (discipline is null) {
					continue;
				}
				var insert =
					new Insert(_connectionString)
						.Into("SysStudentDiscipline")
							.Set("StudentId", newStudentId)
							.Set("DisciplineId", discipline.Id) as Insert;
				insert.Execute();
			}
		}

		private void InsertStudent(string name, string cardNum, AuthObject authObject, Guid newStudentId) {
			var insert =
				new Insert(_connectionString)
					.Into("Student")
						.Set("Id", newStudentId)
						.Set("Name", name)
						.Set("StudentCardNumber", cardNum)
						.Set("PasswordHash", authObject.PasswordHash)
						.Set("Salt", authObject.Salt)
						.Set("IsAdmin", _parent is LoginForm lf && lf.IsAdminForm) as Insert;
			bool insertSuccess = insert.Execute();
			if (!insertSuccess) {
				throw new Exception("Не вдалося створити запис");
			}
		}

		private void LoadUserTabDisciplines() {
			clb_disciplines.SelectedItem = null;
			Discipline[] disciplines = Helper.GetDisciplineList(_connectionString);
			clb_disciplines.Items.Clear();
			clb_disciplines.Items.AddRange(disciplines);
			if (_state == State.Edit) {
				CheckSelectedDisciplines(_entity);
			}
		}

		private void CheckSelectedDisciplines(Student student) {
			if (student is null) {
				return;
			}
			Discipline[] selectedDisciplines = Helper.GetDisciplineList(_connectionString, student.Id);
			for (int i = 0; i < clb_disciplines.Items.Count; i++) {
				object item = clb_disciplines.Items[i];
				if (item is Discipline discipline && selectedDisciplines.Contains(discipline)) {
					clb_disciplines.SetItemChecked(i, true);
				}
			}
		}

		public override void SetData(State state, Student entity = null) {
			base.SetData(state, entity);
			if (entity is null) {
				tb_password.Visible = l_password.Visible = true;
				tb_name.Text = tb_cardNum.Text = tb_password.Text = String.Empty;
				return;
			}
			tb_password.Visible = l_password.Visible = false;
			tb_name.Text = entity.Name;
			tb_cardNum.Text = entity.CardNum;
		}
	}
}
