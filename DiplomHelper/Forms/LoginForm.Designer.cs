﻿namespace DiplomHelper
{
	partial class LoginForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.tbLogin = new System.Windows.Forms.TextBox();
			this.tbPassword = new System.Windows.Forms.TextBox();
			this.bLogin = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.b_register = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbLogin
			// 
			this.tbLogin.Location = new System.Drawing.Point(192, 48);
			this.tbLogin.Name = "tbLogin";
			this.tbLogin.Size = new System.Drawing.Size(100, 20);
			this.tbLogin.TabIndex = 0;
			this.tbLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginPassword_KeyDown);
			// 
			// tbPassword
			// 
			this.tbPassword.Location = new System.Drawing.Point(192, 94);
			this.tbPassword.Name = "tbPassword";
			this.tbPassword.PasswordChar = '*';
			this.tbPassword.Size = new System.Drawing.Size(100, 20);
			this.tbPassword.TabIndex = 1;
			this.tbPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginPassword_KeyDown);
			// 
			// bLogin
			// 
			this.bLogin.Location = new System.Drawing.Point(61, 157);
			this.bLogin.Name = "bLogin";
			this.bLogin.Size = new System.Drawing.Size(75, 23);
			this.bLogin.TabIndex = 2;
			this.bLogin.Text = "Увійти";
			this.bLogin.UseVisualStyleBackColor = true;
			this.bLogin.Click += new System.EventHandler(this.bLogin_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(32, 51);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(156, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Номер студентського квитка";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(143, 97);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(45, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Пароль";
			// 
			// b_register
			// 
			this.b_register.Location = new System.Drawing.Point(174, 157);
			this.b_register.Name = "b_register";
			this.b_register.Size = new System.Drawing.Size(109, 23);
			this.b_register.TabIndex = 5;
			this.b_register.Text = "Зареєструватися";
			this.b_register.UseVisualStyleBackColor = true;
			this.b_register.Click += new System.EventHandler(this.b_register_Click);
			// 
			// LoginForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(361, 244);
			this.Controls.Add(this.b_register);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.bLogin);
			this.Controls.Add(this.tbPassword);
			this.Controls.Add(this.tbLogin);
			this.Name = "LoginForm";
			this.Text = "Форма авторизації";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		protected System.Windows.Forms.TextBox tbLogin;
		protected System.Windows.Forms.TextBox tbPassword;
		protected System.Windows.Forms.Button bLogin;
		protected System.Windows.Forms.Label label1;
		protected System.Windows.Forms.Label label2;
		protected System.Windows.Forms.Button b_register;
	}
}