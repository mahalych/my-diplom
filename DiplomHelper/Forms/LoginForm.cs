﻿using System;
using System.Data;
using System.Windows.Forms;
using Diplom.EntityForms;
using DiplomHelper.Db;
using DiplomHelper.Enums;

namespace DiplomHelper
{
	public partial class LoginForm : BaseForm
	{
		protected readonly BaseForm _mainForm;
		protected readonly StudentEntityForm _studentEntityForm;

		public LoginForm() => InitializeComponent();

		public LoginForm(string connectionString, Type mainFormType) : base(connectionString) {
			InitializeComponent();
			_mainForm = Activator.CreateInstance(mainFormType, connectionString) as BaseForm;
			_studentEntityForm = new StudentEntityForm(this, connectionString);
		}

		public virtual bool IsAdminForm => false;

		protected async virtual void bLogin_Click(object sender, EventArgs e) {
			LoginData loginData = await Helper.ShowMask(this, GetLoginData);
			if (loginData is null) {
				return;
			}
			string password = tbPassword.Text;
			AuthObject authObject = Helper.GetAuthObject(tbLogin.Text, password, loginData.Salt);
			if (authObject.PasswordHash != loginData.PasswordHash || !IsLoginGranted(loginData)) {
				MessageBox.Show("Неправильний логін або пароль",
					"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			tbLogin.Text = tbPassword.Text = String.Empty;
			_mainForm.UserId = loginData.UserId;
			_mainForm.Show();
			Hide();
		}

		protected virtual LoginData GetLoginData() {
			string login = tbLogin.Text;
			var select =
				new Select(_connectionString).Top(1)
					.Column<Guid>("Id")
					.Column<string>("PasswordHash")
					.Column<string>("Salt")
					.Column<bool>("IsAdmin")
				.From("Student")
				.Where("Student", "StudentCardNumber").IsEqual(login) as Select;
			DataTable selectResult = select.Execute();
			if (selectResult.Rows.Count <= 0) {
				MessageBox.Show("Неправильний логін або пароль",
					"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
			DataRow userRow = selectResult.Rows[0];
			return new LoginData {
				UserId = (Guid)userRow["Id"],
				PasswordHash = (string)userRow["PasswordHash"],
				Salt = (string)userRow["Salt"],
				IsAdmin = (bool)userRow["IsAdmin"]
			};
		}

		protected virtual bool IsLoginGranted(LoginData loginData) => false;

		protected virtual void LoginPassword_KeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				bLogin_Click(sender, e);
			}
		}

		protected virtual void b_register_Click(object sender, EventArgs e) {
			_studentEntityForm.SetData(State.Add);
			_studentEntityForm.Show();
		}
	}
}
