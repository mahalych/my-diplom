﻿using System;
using System.Windows.Forms;

namespace DiplomHelper
{
	public partial class BaseForm : Form
	{
		protected readonly string _connectionString;

		public Guid UserId {
			get; set;
		}

		public BaseForm() {
			InitializeComponent();
		}

		public BaseForm(string connectionString) : this()
			=> _connectionString = connectionString;

		private void BaseForm_FormClosed(object sender, FormClosedEventArgs e) {
			Application.Exit();
		}
	}
}
