﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiplomHelper.Forms
{
	public partial class LoadForm : Form
	{
		public LoadForm() {
			InitializeComponent();
			BeforeShowMask += OnBeforeShowMask;
			BeforeHideMask += OnBeforeHideMask;
		}

		public event Action<MaskEventArgs> BeforeShowMask;
		public event Action<MaskEventArgs> BeforeHideMask;

		protected virtual void OnBeforeShowMask(MaskEventArgs args) {
			Form parent = args.ParentForm;
			parent?.Hide();
			Show();
		}

		protected virtual void OnBeforeHideMask(MaskEventArgs args) {
			Form parent = args.ParentForm;
			Hide();
			parent?.Show();
		}

		public async Task ShowMask(Form parent, Action action) {
			var args = new MaskEventArgs(parent);
			BeforeShowMask(args);
			await Task.Run(action);
			BeforeHideMask(args);
		}

		public async Task<T> ShowMask<T>(Form parent, Func<T> func) {
			var args = new MaskEventArgs(parent);
			BeforeShowMask(args);
			T result = await Task.Run(func);
			BeforeHideMask(args);
			return await Task.FromResult(result);
		}

		private void LoadForm_FormClosed(object sender, FormClosedEventArgs e) {
			Application.Exit();
		}
	}
}
