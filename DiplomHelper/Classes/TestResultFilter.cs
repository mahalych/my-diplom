﻿using System;

namespace DiplomHelper
{
	public class TestResultFilter
	{
		public Guid StudentId {
			get; set;
		}
		public Guid DisciplineId {
			get; set;
		}
		public Guid LabWorkId {
			get; set;
		}
		public DateTime StartDate {
			get; set;
		}
		public DateTime EndDate {
			get; set;
		}
		public bool DescSort {
			get; set;
		}
	}
}
