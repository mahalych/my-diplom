﻿using System;

namespace DiplomHelper
{
	public class LoginData
	{
		public Guid UserId {
			get; set;
		}
		public string PasswordHash {
			get; set;
		}
		public string Salt {
			get; set;
		}
		public bool IsAdmin {
			get; set;
		}
	}
}
