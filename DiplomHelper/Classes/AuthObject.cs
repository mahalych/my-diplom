﻿namespace DiplomHelper
{
	public class AuthObject
	{
		public string PasswordHash {
			get; set;
		}
		public string Salt {
			get; set;
		}
	}
}
