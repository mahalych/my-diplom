﻿using System;

namespace DiplomHelper
{
	public class TestResult
	{
		public string StudentName {
			get; set;
		}
		public string StudCardNumber {
			get; set;
		}
		public string DisciplineName {
			get; set;
		}
		public int LabWorkNumber {
			get; set;
		}
		public string LabWorkName {
			get; set;
		}
		public decimal Grade {
			get; set;
		}
		public DateTime ExamDate {
			get; set;
		}

		public override string ToString() => $"{DisciplineName} - {LabWorkNumber} - {LabWorkName} - " +
			$"{StudCardNumber} - {StudentName} - {ExamDate.ToString("dd.MM.yyyy HH:mm:ss")} - {Grade}%";
	}
}
