﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DiplomHelper.Db;
using DiplomHelper.Entities;
using DiplomHelper.Forms;

namespace DiplomHelper
{
	public static class Helper
	{
		private static readonly LoadForm _loadForm = new LoadForm();

		public static Task ShowMask(Form parent, Action action) => _loadForm.ShowMask(parent, action);

		public static Task<T> ShowMask<T>(Form parent, Func<T> func) => _loadForm.ShowMask(parent, func);

		private static Select GetTestResultBaseSelect(string connectionString, int? top) {
			var select = new Select(connectionString);
			if (top != null) {
				select.Top((int)top);
			}
			return ((select.Column<string>("Student", "Name")
					.Column<string>("Student", "StudentCardNumber")
					.Column<string>("LaboratoryWork", "Name")
					.Column<int>("LaboratoryWork", "Number")
					.Column<string>("Discipline", "Name")
					.Column<string>("SysStudentLabWorkGrades", "Grade")
					.Column<DateTime>("SysStudentLabWorkGrades", "CreatedOn")
				.From("SysStudentLabWorkGrades")
				.Join("Student", JoinType.Inner)
					.On("Student", "Id").IsEqual("SysStudentLabWorkGrades", "StudentId") as Select)
				.Join("LaboratoryWork", JoinType.Inner)
					.On("LaboratoryWork", "Id").IsEqual("SysStudentLabWorkGrades", "LaboratoryWorkId") as Select)
				.Join("Discipline", JoinType.Inner)
					.On("Discipline", "Id").IsEqual("LaboratoryWork", "DisciplineId") as Select;
		}

		private static string GenerateSalt() {
			var rng = new RNGCryptoServiceProvider();
			byte[] buffer = new byte[256];
			rng.GetNonZeroBytes(buffer);
			return Convert.ToBase64String(buffer);
		}

		public static byte[] GetPasswordHash(string cardNum, string password, string salt) {
			var sha1 = SHA256.Create();
			string loginAndPass = $"{cardNum};{password}{salt}";
			byte[] utf8LoginAndPass = Encoding.UTF8.GetBytes(loginAndPass);
			byte[] hash = sha1.ComputeHash(utf8LoginAndPass);
			return hash;
		}

		public static AuthObject GetAuthObject(string cardNum, string password, string salt = null) {
			if (salt is null) {
				salt = GenerateSalt();
			}
			byte[] hash = GetPasswordHash(cardNum, password, salt);
			return new AuthObject {
				PasswordHash = Convert.ToBase64String(hash),
				Salt = salt
			};
		}

		public static Discipline[] GetDisciplineList(string connectionString, Guid userId) {
			var selectDiscplines =
				new Select(connectionString)
					.Column<Guid>("Discipline", "Id")
					.Column<string>("Discipline", "Name")
				.From("Discipline")
				.Join("SysStudentDiscipline", JoinType.Inner)
					.On("Discipline", "Id")
						.IsEqual("SysStudentDiscipline", "DisciplineId")
				.Where("SysStudentDiscipline", "StudentId")
					.IsEqual(userId) as Select;
			DataTable disciplines = selectDiscplines.Execute();
			var discList = new List<Discipline>();
			foreach (DataRow row in disciplines.Rows) {
				var discId = (Guid)row["[Discipline].[Id]"];
				string discName = (string)row["[Discipline].[Name]"];
				var discipline = new Discipline(discId, discName);
				discList.Add(discipline);
			}
			return discList.ToArray();
		}

		public static Discipline[] GetDisciplineList(string connectionString) {
			var selectDiscplines =
				new Select(connectionString)
					.Column<Guid>("Id")
					.Column<string>("Name")
				.From("Discipline") as Select;
			DataTable disciplines = selectDiscplines.Execute();
			var discList = new List<Discipline>();
			foreach (DataRow row in disciplines.Rows) {
				var discId = (Guid)row["Id"];
				string discName = (string)row["Name"];
				var discipline = new Discipline(discId, discName);
				discList.Add(discipline);
			}
			return discList.ToArray();
		}

		public static LaboratoryWork[] GetLaboratoryWorks(string connectionString,
				Guid disciplineId) {
			var selectLabs =
				new Select(connectionString)
					.Column<Guid>("Id")
					.Column<string>("Name")
					.Column<int>("Number")
				.From("LaboratoryWork")
				.Where("LaboratoryWork", "DisciplineId")
					.IsEqual(disciplineId)
				.OrderByAsc("Number") as Select;
			DataTable labs = selectLabs.Execute();
			var labsList = new List<LaboratoryWork>();
			foreach (DataRow row in labs.Rows) {
				var labId = (Guid)row["Id"];
				string labName = (string)row["Name"];
				int number = (int)row["Number"];
				var discipline = new LaboratoryWork(labId, labName, disciplineId, number);
				labsList.Add(discipline);
			}
			return labsList.ToArray();
		}

		public static Student[] GetStudents(string connectionString, bool includeAdmins) {
			var selectDiscplines =
				new Select(connectionString)
					.Column<Guid>("Id")
					.Column<string>("Name")
					.Column<string>("StudentCardNumber")
					.Column<string>("PasswordHash")
				.From("Student") as Select;
			if (!includeAdmins) {
				selectDiscplines.Where("IsAdmin").IsEqual(false);
			}
			DataTable disciplines = selectDiscplines.Execute();
			var result = new List<Student>();
			foreach (DataRow row in disciplines.Rows) {
				var id = (Guid)row["Id"];
				string name = (string)row["Name"];
				string cardNum = (string)row["StudentCardNumber"];
				string hash = (string)row["PasswordHash"];
				var student = new Student(id, name, cardNum, hash);
				result.Add(student);
			}
			return result.ToArray();
		}

		public static Question[] GetQuestions(string connectionString, Guid labWorkId) {
			var select =
				new Select(connectionString)
					.Column<Guid>("Id")
					.Column<string>("Text")
					.Column<decimal>("Cost")
				.From("Question")
				.Where("LaboratoryWorkId").IsEqual(labWorkId) as Select;
			DataTable questions = select.Execute();
			var result = new List<Question>();
			foreach (DataRow row in questions.Rows) {
				var id = (Guid)row["Id"];
				string text = (string)row["Text"];
				decimal cost = (decimal)row["Cost"];
				var question = new Question(id, text, labWorkId, cost);
				result.Add(question);
			}
			return result.ToArray();
		}

		public static AnswerVariant[] GetAnswerVariants(string connectionString, Guid questionId) {
			var select =
				new Select(connectionString)
					.Column<Guid>("Id")
					.Column<string>("Text")
					.Column<bool>("IsCorrect")
				.From("AnswerVariant")
				.Where("QuestionId").IsEqual(questionId) as Select;
			DataTable questions = select.Execute();
			var result = new List<AnswerVariant>();
			foreach (DataRow row in questions.Rows) {
				var id = (Guid)row["Id"];
				string text = (string)row["Text"];
				bool isCorrect = (bool)row["IsCorrect"];
				var variant = new AnswerVariant(id, text, questionId, isCorrect);
				result.Add(variant);
			}
			return result.ToArray();
		}

		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action) {
			if (source == null) {
				throw new ArgumentNullException(nameof(source));
			}
			if (action == null) {
				throw new ArgumentNullException(nameof(action));
			}
			foreach (T item in source) {
				action(item);
			}
		}

		public static string EncryptString(string input, byte[] key) {
			if (key.Length != 32) {
				throw new ArgumentException("length must be 32", nameof(key));
			}
			var aes = new AesCryptoServiceProvider() {
				Key = key,
				IV = key.Reverse().Take(16).ToArray()
			};
			using (var ms = new MemoryStream()) {
				using (var cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write)) {
					using (var sw = new StreamWriter(cs)) {
						sw.Write(input);
					}
					return Convert.ToBase64String(ms.ToArray());
				}
			}
		}

		public static string DecryptString(string input, byte[] key) {
			if (key.Length != 32) {
				throw new ArgumentException("length must be 32", nameof(key));
			}
			var aes = new AesCryptoServiceProvider() {
				Key = key,
				IV = key.Reverse().Take(16).ToArray()
			};
			using (var ms = new MemoryStream(Convert.FromBase64String(input))) {
				using (var cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Read)) {
					using (var sr = new StreamReader(cs)) {
						return sr.ReadToEnd();
					}
				}
			}
		}

		public static Task<TestResult[]> GetTestResults(string connectionString,
				TestResultFilter testResultFilter, byte[] key, int? top = null) =>
			Task.Run(() => {
				Select select = GetTestResultBaseSelect(connectionString, top);
				if (testResultFilter.StudentId != Guid.Empty) {
					select.Where("Student", "Id").IsEqual(testResultFilter.StudentId);
				}
				if (testResultFilter.DisciplineId != Guid.Empty) {
					if (select.SqlText.Contains("WHERE")) {
						select.And("Discipline", "Id").IsEqual(testResultFilter.DisciplineId);
					} else {
						select.Where("Discipline", "Id").IsEqual(testResultFilter.DisciplineId);
					}
				}
				if (testResultFilter.LabWorkId != Guid.Empty) {
					if (select.SqlText.Contains("WHERE")) {
						select.And("LaboratoryWork", "Id").IsEqual(testResultFilter.LabWorkId);
					} else {
						select.Where("LaboratoryWork", "Id").IsEqual(testResultFilter.LabWorkId);
					}
				}
				if (select.SqlText.Contains("WHERE")) {
					select.And(new Cast("SysStudentLabWorkGrades", "CreatedOn", Cast.CastType.Date))
						.IsBetween(
							new Cast(testResultFilter.StartDate, Cast.CastType.Date),
							new Cast(testResultFilter.EndDate, Cast.CastType.Date)
						);
				} else {
					select.Where(new Cast("SysStudentLabWorkGrades", "CreatedOn", Cast.CastType.Date))
						.IsBetween(
							new Cast(testResultFilter.StartDate, Cast.CastType.Date),
							new Cast(testResultFilter.EndDate, Cast.CastType.Date)
						);
				}
				if (testResultFilter.DescSort) {
					select.OrderByDesc("SysStudentLabWorkGrades", "CreatedOn");
				} else {
					select.OrderByAsc("SysStudentLabWorkGrades", "CreatedOn");
				}
				DataTable resultsTable = select.Execute();
				var result = new List<TestResult>();
				foreach (DataRow row in resultsTable.Rows) {
					string studentName = (string)row[0];
					string studentCardNumber = (string)row[1];
					string labWorkName = (string)row[2];
					int labWorkNumber = (int)row[3];
					string disciplineName = (string)row[4];
					decimal grade = Decimal.Parse(DecryptString((string)row[5], key));
					var examDate = (DateTime)row[6];
					result.Add(new TestResult {
						DisciplineName = disciplineName,
						ExamDate = examDate,
						Grade = grade,
						LabWorkName = labWorkName,
						LabWorkNumber = labWorkNumber,
						StudCardNumber = studentCardNumber,
						StudentName = studentName
					});
				}
				return result.ToArray();
			});

		public static void LoadDisciplinesToComboBox(string connectionString, ComboBox comboBox) {
			comboBox.SelectedItem = null;
			Discipline[] disciplines = GetDisciplineList(connectionString);
			comboBox.Items.Clear();
			comboBox.Items.AddRange(disciplines);
		}

		public static void LoadDisciplinesLabWorksToComboBox(string connectionString,
				ComboBox comboBox, Guid disciplineId) {
			comboBox.SelectedItem = null;
			LaboratoryWork[] labs = GetLaboratoryWorks(connectionString,
				disciplineId);
			comboBox.Items.Clear();
			comboBox.Items.AddRange(labs);
		}

		public static void LoadStudentsToComboBox(string connectionString, ComboBox comboBox,
				bool includeAdmins = false) {
			comboBox.SelectedItem = null;
			Student[] students = GetStudents(connectionString, includeAdmins);
			comboBox.Items.Clear();
			comboBox.Items.AddRange(students);
		}

		public static void InsertQuestion(string connectionString, Guid labWorkId,
				string questionText, decimal cost, Guid newQuestionId) {
			var insertQuestion =
				new Insert(connectionString).Into("Question")
					.Set("Id", newQuestionId)
					.Set("Text", questionText)
					.Set("Cost", cost)
					.Set("LaboratoryWorkId", labWorkId) as Insert;
			bool successAddQuestion = insertQuestion.Execute();
			if (!successAddQuestion) {
				throw new Exception("Не вдалося додати запитання");
			}
		}

		public static void InsertAnswerVariants(string connectionString, Guid newQuestionId,
				IEnumerable<AnswerVariantShell> variants, byte[] key) {
			foreach (AnswerVariantShell variant in variants.Take(4)) {
				if (String.IsNullOrEmpty(variant.Text)) {
					break;
				}
				var insertVariant =
					new Insert(connectionString).Into("AnswerVariant")
						.Set("Text", EncryptString(variant.Text, key))
						.Set("QuestionId", newQuestionId)
						.Set("IsCorrect", variant.IsCorrect) as Insert;
				insertVariant.Execute();
			}
		}
	}
}
