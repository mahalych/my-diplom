﻿using System;
using System.Windows.Forms;

namespace DiplomHelper
{
	public class MaskEventArgs : EventArgs
	{
		public MaskEventArgs() { }

		public MaskEventArgs(Form parentForm) => ParentForm = parentForm;

		public Form ParentForm {
			get;
		}
	}
}
