﻿namespace DiplomHelper
{
	public class AnswerVariantShell
	{
		public string Text {
			get; set;
		}
		public bool IsCorrect {
			get; set;
		}
	}
}
