﻿namespace DiplomHelper
{
	public class QuestionImport
	{
		public string Discipline {
			get; set;
		}
		public int LabWork {
			get; set;
		}
		public string QuestionText {
			get; set;
		}
		public decimal Cost {
			get; set;
		}
		public AnswerVariantShell[] AnswerVariants {
			get; set;
		}
	}
}
