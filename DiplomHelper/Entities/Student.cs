﻿using System;

namespace DiplomHelper.Entities
{
	public class Student
	{
		public Student(Guid id, string name, string cardNum, string passwordHash) {
			Id = id;
			Name = name ?? throw new ArgumentNullException(nameof(name));
			CardNum = cardNum ?? throw new ArgumentNullException(nameof(cardNum));
			PasswordHash = passwordHash ?? throw new ArgumentNullException(nameof(passwordHash));
		}

		public Guid Id {
			get;
		}
		public string Name {
			get;
		}
		public string CardNum {
			get;
		}
		public string PasswordHash {
			get;
		}

		public override string ToString() => $"{Name} - {CardNum}";
	}
}
