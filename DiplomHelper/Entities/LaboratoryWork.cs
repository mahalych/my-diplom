﻿using System;

namespace DiplomHelper.Entities
{
	public class LaboratoryWork
	{
		public LaboratoryWork(Guid id, string name, Guid disciplineId, int number) {
			Id = id;
			Name = name ?? throw new ArgumentNullException(nameof(name));
			DisciplineId = disciplineId;
			Number = number;
		}

		public Guid Id {
			get;
		}
		public string Name {
			get;
		}
		public Guid DisciplineId {
			get;
		}
		public int Number {
			get;
		}

		public override string ToString() => $"{Number}.{Name}";
	}
}
