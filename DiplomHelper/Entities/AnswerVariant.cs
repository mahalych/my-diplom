﻿using System;
using System.Collections.Generic;

namespace DiplomHelper.Entities
{
	public class AnswerVariant
	{
		public AnswerVariant(Guid id, string text, Guid questionId, bool isCorrect) {
			Id = id;
			Text = text ?? throw new ArgumentNullException(nameof(text));
			QuestionId = questionId;
			IsCorrect = isCorrect;
		}

		public Guid Id {
			get;
		}
		public string Text {
			get;
		}
		public Guid QuestionId {
			get;
		}
		public bool IsCorrect {
			get;
		}

		public override string ToString() => Text;

		public override bool Equals(object obj) {
			if (obj is null) {
				return false;
			}
			if (obj is AnswerVariant answ) {
				return answ.Id == Id;
			}
			return false;
		}

		public override int GetHashCode() => 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
	}
}
