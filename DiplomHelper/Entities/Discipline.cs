﻿using System;
using System.Collections.Generic;

namespace DiplomHelper.Entities
{
	public class Discipline
	{
		public Discipline(Guid id, string name) {
			Id = id;
			Name = name ?? throw new ArgumentNullException(nameof(name));
		}

		public Guid Id {
			get;
		}
		public string Name {
			get;
		}

		public override bool Equals(object obj) => obj is Discipline discipline && Id.Equals(discipline.Id);
		public override int GetHashCode() => 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
		public override string ToString() => Name;

	}
}
