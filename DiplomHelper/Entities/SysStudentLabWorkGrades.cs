﻿using System;

namespace DiplomHelper.Entities
{
	public class SysStudentLabWorkGrades
	{
		public SysStudentLabWorkGrades(Guid id, DateTime createdOn, Guid studentId, Guid labWorkId, decimal grade) {
			Id = id;
			CreateOn = createdOn;
			StudentId = studentId;
			LabWorkId = labWorkId;
			Grade = grade;
		}

		public Guid Id {
			get;
		}
		public DateTime CreateOn {
			get;
		}
		public Guid StudentId {
			get;
		}
		public Guid LabWorkId {
			get;
		}
		public decimal Grade {
			get;
		}
	}
}
