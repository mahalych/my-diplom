﻿using System;
using System.Collections.Generic;

namespace DiplomHelper.Entities
{
	public class Question
	{
		public Question(Guid id, string text, Guid labWorkId, decimal cost) {
			Id = id;
			Text = text ?? throw new ArgumentNullException(nameof(text));
			LabWorkId = labWorkId;
			Cost = cost;
			Answers = new List<AnswerVariant>();
		}

		public Guid Id {
			get;
		}
		public string Text {
			get;
		}
		public Guid LabWorkId {
			get;
		}
		public decimal Cost {
			get;
		}
		public List<AnswerVariant> Answers {
			get;
		}

		public override string ToString() => Text;

		public override bool Equals(object obj) {
			if (obj is null) {
				return false;
			}
			if (obj is Question quest) {
				return quest.Id == Id;
			}
			return false;
		}

		public override int GetHashCode() => 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
	}
}
