﻿using System;

namespace DiplomHelper.Entities
{
	public class SysStudentDiscipline
	{
		public SysStudentDiscipline(Guid id, Guid studentId, Guid disciplineId) {
			Id = id;
			StudentId = studentId;
			DisciplineId = disciplineId;
		}

		public Guid Id {
			get;
		}
		public Guid StudentId {
			get;
		}
		public Guid DisciplineId {
			get;
		}

	}
}
