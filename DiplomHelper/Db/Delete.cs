﻿using System;
using System.Data.SqlClient;

namespace DiplomHelper.Db
{
	public class Delete : Query
	{
		public Delete(string connectionString, string tableName) : base(connectionString) {
			if (String.IsNullOrEmpty(tableName)) {
				throw new ArgumentNullException(nameof(tableName));
			}
			_sqlText.AppendLine($"DELETE FROM [{tableName}]");
		}

		public bool Execute() {
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(SqlText, connection)) {
					int result = command.ExecuteNonQuery();
					return result > 0;
				}
			}
		}
	}
}
