﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomHelper.Db
{
	public class CustomQuery
	{
		private string _connectionString;
		private string _sqlText;

		public CustomQuery(string connectionString, string sqlText) {
			_connectionString = connectionString;
			_sqlText = sqlText;
		}

		private DataTable GetDataTable(params Type[] types) {
			var result = new DataTable();
			for (int i = 0; i < types.Length; i++) {
				var column = new DataColumn($"Column{i}", types[i]);
				result.Columns.Add(column);
			}
			return result;
		}

		public DataTable Execute(params Type[] types) {
			DataTable result = null;
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(_sqlText, connection)) {
					SqlDataReader reader = command.ExecuteReader();
					result = GetDataTable(types);
					while (reader.Read()) {
						DataRow row = result.NewRow();
						for (int i = 0; i < result.Columns.Count; i++) {
							row[i] = reader[i];
						}
						result.Rows.Add(row);
					}
					return result;
				}
			}
		}

		public object ExecuteScalar() {
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(_sqlText, connection)) {
					object result = command.ExecuteScalar();
					return result;
				}
			}
		}

		public T ExecuteScalar<T>() {
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(_sqlText, connection)) {
					object result = command.ExecuteScalar();
					return result is null ? default : (T)result;
				}
			}
		}
	}
}
