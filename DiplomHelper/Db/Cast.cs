﻿using System.Collections.Generic;

namespace DiplomHelper.Db
{
	public class Cast
	{
		public enum CastType
		{
			Int,
			Date,
			DateTime,
			Uniqueidentifier,
			ShortString,
			MiddleString,
			LongString,
			UnlimitedLengthString
		}

		private static readonly Dictionary<CastType, string> _keyValuePairs = new Dictionary<CastType, string> {
			[CastType.Int] = "INT",
			[CastType.Date] = "DATE",
			[CastType.DateTime] = "DATETIME2",
			[CastType.Uniqueidentifier] = "UNIQUEIDENTIFIER",
			[CastType.ShortString] = "NVARCHAR(50)",
			[CastType.MiddleString] = "NVARCHAR(250)",
			[CastType.LongString] = "NVARCHAR(500)",
			[CastType.UnlimitedLengthString] = "NVARCHAR(MAX)"
		};

		public Cast(string tableName, string columnName, CastType type) {
			TableName = tableName;
			ColumnName = columnName;
			Type = type;
		}

		public Cast(string columnName, CastType type) {
			ColumnName = columnName;
			Type = type;
		}

		public Cast(object value, CastType type) {
			Value = value;
			Type = type;
		}

		public string TableName {
			get; set;
		}
		public string ColumnName {
			get; set;
		}
		public CastType Type {
			get; set;
		}
		public object Value {
			get; set;
		}

		public static string GetStringType(CastType castType) => _keyValuePairs[castType];
	}
}
