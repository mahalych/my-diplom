﻿namespace DiplomHelper.Db
{
	public class Upper
	{
		public Upper(string tableName, string columnName) {
			TableName = tableName;
			ColumnName = columnName;
		}

		public Upper(string columnName) {
			ColumnName = columnName;
		}

		public string TableName {
			get; set;
		}
		public string ColumnName {
			get; set;
		}
	}
}
