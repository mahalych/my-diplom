﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DiplomHelper.Db
{
	public class Select : Query
	{
		private readonly List<(string name, Type type)> _columns = new List<(string name, Type type)>();
		private readonly Dictionary<JoinType, string> _joins = new Dictionary<JoinType, string> {
			{ JoinType.Inner, "INNER JOIN " },
			{ JoinType.Left, "LEFT OUTER JOIN " },
			{ JoinType.Right, "RIGHT OUTER JOIN " },
			{ JoinType.FullOuterJoin, "FULL OUTER JOIN " },
			{ JoinType.Cross, "CROSS JOIN " }
		};

		public Select(string connectionString) : base(connectionString) {
			_sqlText.AppendLine("SELECT");
		}

		private void CreateTableColumns(DataTable dt) {
			foreach ((string name, Type type) in _columns) {
				var column = new DataColumn(name, type);
				dt.Columns.Add(column);
			}
		}

		public Select Column<T>(string columnName) {
			if (_columns.Any()) {
				_sqlText.Append(", ");
			} 
			_sqlText.AppendLine(columnName);
			_columns.Add((columnName, typeof(T)));
			return this;
		}

		public Select Top(int num) {
			if (num > 0) {
				_sqlText.AppendLine($" TOP {num}"); 
			}
			return this;
		}

		public Select Column<T>(string tableName, string columnName) {
			if (_columns.Any()) {
				_sqlText.Append(", ");
			}
			_sqlText.AppendLine($"[{tableName}].[{columnName}]");
			_columns.Add(($"[{tableName}].[{columnName}]", typeof(T)));
			return this;
		}

		public Select From(string tableName) {
			_sqlText.AppendLine($" FROM [dbo].[{tableName}] WITH (NOLOCK) ");
			return this;
		}

		public Select Join(string secondTableName, JoinType joinType) {
			string join = _joins[joinType];
			_sqlText.AppendLine($" {join}[{secondTableName}] WITH (NOLOCK) ");
			return this;
		}

		public Select On(string firstTableName, string firstTableColumnName) {
			_sqlText.Append($" ON [{firstTableName}].[{firstTableColumnName}] ");
			return this;
		}

		public DataTable Execute() {
			DataTable result = null;
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(SqlText, connection)) {
					SqlDataReader reader = command.ExecuteReader();
					result = new DataTable();
					CreateTableColumns(result);
					while (reader.Read()) {
						DataRow row = result.NewRow();
						for (int i = 0; i < result.Columns.Count; i++) {
							row[i] = reader[i];
						}
						result.Rows.Add(row);
					}
					return result;
				}
			}
		}

		public object ExecuteScalar() {
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(SqlText, connection)) {
					object result = command.ExecuteScalar();
					return result;
				}
			}
		}

		public T ExecuteScalar<T>() {
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(SqlText, connection)) {
					object result = command.ExecuteScalar();
					return result is null ? default : (T)result;
				}
			}
		}
	}
}
