﻿using System;
using System.Data.SqlClient;
using System.Linq;

namespace DiplomHelper.Db
{
	public class Update : Query
	{
		public Update(string connectionString, string tableName) : base(connectionString) {
			if (String.IsNullOrEmpty(tableName)) {
				throw new ArgumentNullException(nameof(tableName));
			}
			_sqlText.AppendLine($"UPDATE [{tableName}]");
		}

		private void SetParameters() {
			if (!Parameters.Any()) {
				throw new ArgumentException("nothing to update", nameof(Parameters));
			}
			string paramString = String.Join(",", Parameters.Select(param => $"[{param.Name}] = {ValueToStr(param.Value)}"));
			_sqlText.Append($" SET {paramString}");
		}

		public Update Set(string columnName, object value) {
			Parameters.Add(new QueryParameter {
				Name = columnName,
				Value = value
			});
			return this;
		}

		public override Query Where() {
			SetParameters();
			return base.Where();
		}

		public override Query Where(string tableColumnName) {
			SetParameters();
			return base.Where(tableColumnName);
		}

		public override Query Where(string tableName, string tableColumnName) {
			SetParameters();
			return base.Where(tableName, tableColumnName);
		}

		public bool Execute() {
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(SqlText, connection)) {
					int result = command.ExecuteNonQuery();
					return result > 0;
				}
			}
		}
	}
}
