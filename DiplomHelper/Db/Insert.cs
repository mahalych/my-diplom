﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DiplomHelper.Db
{
	public class Insert : Query
	{
		private List<object> _values = new List<object>();

		public Insert(string connectionString) : base(connectionString) {
			_sqlText.Append("INSERT INTO ");
		}

		public Insert Into(string tableName) {
			_sqlText.AppendLine($"[{tableName}]");
			return this;
		}

		public Insert Set(string columnName, object value) {
			if (!_values.Any()) {
				_sqlText.Append("(");
			} else {
				_sqlText.Append(",");
			}
			_sqlText.AppendLine(columnName);
			_values.Add(value);
			return this;
		}

		public bool Execute() {
			FinishInsertQuery();
			using (var connection = new SqlConnection(_connectionString)) {
				connection.Open();
				using (var command = new SqlCommand(SqlText, connection)) {
					int result = command.ExecuteNonQuery();
					return result > 0;
				}
			}
		}

		private void FinishInsertQuery() {
			_sqlText.AppendLine(") VALUES (");
			bool isFirst = true;
			foreach (object value in _values) {
				if (!isFirst) {
					_sqlText.Append(",");
				}
				_sqlText.Append(ValueToStr(value));
				if (isFirst) {
					isFirst = false;
				}
			}
			_sqlText.Append(");");
		}
	}
}
