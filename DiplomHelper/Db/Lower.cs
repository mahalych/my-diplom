﻿namespace DiplomHelper.Db
{
	public class Lower
	{
		public Lower(string tableName, string columnName) {
			TableName = tableName;
			ColumnName = columnName;
		}

		public Lower(string columnName) {
			ColumnName = columnName;
		}

		public string TableName {
			get; set;
		}
		public string ColumnName {
			get; set;
		}
	}
}
