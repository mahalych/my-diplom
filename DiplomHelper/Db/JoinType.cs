﻿namespace DiplomHelper.Db
{
	public enum JoinType
	{
		Inner,
		Left,
		Right,
		FullOuterJoin,
		Cross
	}
}
