﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRes = DiplomHelper.Db.CreateTables.CreateTablesHelperResources;

namespace DiplomHelper.Db.CreateTables
{
	public static class CreateTablesHelper
	{
		public class CreateTableShell
		{
			private string _createTableSqlStatement;

			public string TableName {
				get; set;
			}
			public string CreateTableSqlStatement {
				get => _createTableSqlStatement;
				set => _createTableSqlStatement = value.Replace("\\n", Environment.NewLine);
			}
		}

		public class CreateIndexShell
		{
			public string TableName {
				get; set;
			}
			public string IndexName {
				get; set;
			}
			public string CreateIndexSqlStatement {
				get; set;
			}
		}

		private static List<CreateTableShell> _tableShells = new List<CreateTableShell> {
			new CreateTableShell {
				TableName = "Student",
				CreateTableSqlStatement = MyRes.CreateStudentTable
			},
			new CreateTableShell {
				TableName = "Discipline",
				CreateTableSqlStatement = MyRes.CreateDisciplineTable
			},
			new CreateTableShell {
				TableName = "SysStudentDiscipline",
				CreateTableSqlStatement = MyRes.CreateSysStudentDisciplineTable
			},
			new CreateTableShell {
				TableName = "LaboratoryWork",
				CreateTableSqlStatement = MyRes.CreateLaboratoryWorkTable
			},
			new CreateTableShell {
				TableName = "Question",
				CreateTableSqlStatement = MyRes.CreateQuestionTable
			},
			new CreateTableShell {
				TableName = "AnswerVariant",
				CreateTableSqlStatement = MyRes.CreateAnswerVariantTable
			},
			new CreateTableShell {
				TableName = "SysStudentLabWorkGrades",
				CreateTableSqlStatement = MyRes.CreateSysStudentLabWorkGradesTable
			}
		};
		private static List<CreateIndexShell> _indexShells = new List<CreateIndexShell> {
			new CreateIndexShell {
				TableName = "Student",
				IndexName = "IU_Student_StudentCardNumber",
				CreateIndexSqlStatement = MyRes.CreateIU_Student_StudentCardNumberIndex
			},
			new CreateIndexShell {
				TableName = "Discipline",
				IndexName = "IU_Discipline_Name",
				CreateIndexSqlStatement = MyRes.CreateIU_Discipline_NameIndex
			},
			new CreateIndexShell {
				TableName = "Question",
				IndexName = "I_Question_LaboratoryWorkId",
				CreateIndexSqlStatement = MyRes.CreateI_Question_LaboratoryWorkIdIndex
			},
			new CreateIndexShell {
				TableName = "SysStudentDiscipline",
				IndexName = "IX_SysStudentDiscipline_Discipline",
				CreateIndexSqlStatement = MyRes.CreateIX_SysStudentDiscipline_DisciplineIndex
			},
			new CreateIndexShell {
				TableName = "SysStudentDiscipline",
				IndexName = "IX_SysStudentDiscipline_Student",
				CreateIndexSqlStatement = MyRes.CreateIX_SysStudentDiscipline_StudentIndex
			},
			new CreateIndexShell {
				TableName = "LaboratoryWork",
				IndexName = "I_LaboratoryWork_Discipline",
				CreateIndexSqlStatement = MyRes.CreateI_LaboratoryWork_DisciplineIndex
			},
			new CreateIndexShell {
				TableName = "LaboratoryWork",
				IndexName = "IU_LaboratoryWork_DisciplineNumber",
				CreateIndexSqlStatement = MyRes.CreateIU_LaboratoryWork_DisciplineNumberIndex
			},
			new CreateIndexShell {
				TableName = "AnswerVariant",
				IndexName = "I_AnswerVariant_Question",
				CreateIndexSqlStatement = MyRes.CreateI_AnswerVariant_QuestionIndex
			},
			new CreateIndexShell {
				TableName = "SysStudentLabWorkGrades",
				IndexName = "IX_SysStudentLabWorkGrades_StudentId",
				CreateIndexSqlStatement = MyRes.CreateIX_SysStudentLabWorkGrades_StudentIdIndex
			},
			new CreateIndexShell {
				TableName = "SysStudentLabWorkGrades",
				IndexName = "IX_SysStudentLabWorkGrades_LaboratoryWorkId",
				CreateIndexSqlStatement = MyRes.CreateIX_SysStudentLabWorkGrades_LaboratoryWorkIdIndex
			}
		};

		public static IEnumerable<CreateTableShell> CreateTableShells => _tableShells.AsEnumerable();
		public static IEnumerable<CreateIndexShell> CreateIndexShells => _indexShells.AsEnumerable();
	}
}
