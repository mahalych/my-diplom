﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiplomHelper.Db
{
	public class Query
	{
		protected readonly static Type[] _numericTypes = new Type[] {
			typeof(byte),
			typeof(sbyte),
			typeof(short),
			typeof(ushort),
			typeof(int),
			typeof(uint),
			typeof(long),
			typeof(ulong),
			typeof(float),
			typeof(double),
			typeof(decimal)
		};
		protected readonly List<QueryParameter> Parameters = new List<QueryParameter>();

		protected readonly string _connectionString;
		protected StringBuilder _sqlText = new StringBuilder();
		public string SqlText => _sqlText.ToString();

		public Query(string connectionString) {
			_connectionString = connectionString;
		}

		protected string ValueToStr(object value) {
			string stringToAppend = "";
			if (value is null) {
				stringToAppend = "NULL";
			} else if (value.GetType() == typeof(string)) {
				stringToAppend = $"N'{value.ToString()}'";
			} else if (value.GetType() == typeof(bool)) {
				stringToAppend = $"{((bool)value ? 1 : 0)}";
			} else if (_numericTypes.Contains(value.GetType())) {
				stringToAppend = $"{value.ToString().Replace(',', '.')}";
			} else if (value is DateTime dt) {
				stringToAppend = $"'{dt.ToString("yyyy-MM-dd HH:mm:ss")}'";
			} else {
				stringToAppend = $"'{value.ToString()}'";
			}
			return stringToAppend;
		}

		public Query IsEqual(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" = [{tableName}].[{tableColumnName}]");
			return this;
		}

		public Query IsEqual(object parameter) {
			_sqlText.AppendLine($" = {ValueToStr(parameter)}");
			return this;
		}

		public Query In(IEnumerable<object> collection) {
			string parameters = String.Join(",", collection.Select(e => ValueToStr(e)));
			_sqlText.AppendLine($" IN ({parameters})");
			return this;
		}

		public Query IsNotEqual(object parameter) {
			_sqlText.AppendLine($" <> {ValueToStr(parameter)}");
			return this;
		}

		public Query IsGreaterOrEqual(object parameter) {
			_sqlText.AppendLine($" >= {ValueToStr(parameter)}");
			return this;
		}

		public Query IsGreater(object parameter) {
			_sqlText.AppendLine($" > {ValueToStr(parameter)}");
			return this;
		}

		public Query IsLessOrEqual(object parameter) {
			_sqlText.AppendLine($" <= {ValueToStr(parameter)}");
			return this;
		}

		public Query IsLess(object parameter) {
			_sqlText.AppendLine($" < {ValueToStr(parameter)}");
			return this;
		}

		public Query IsNotEqual(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" <> [{tableName}].[{tableColumnName}]");
			return this;
		}

		public Query IsGreaterOrEqual(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" >= [{tableName}].[{tableColumnName}]");
			return this;
		}

		public Query IsGreater(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" > [{tableName}].[{tableColumnName}]");
			return this;
		}

		public Query IsLessOrEqual(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" <= [{tableName}].[{tableColumnName}]");
			return this;
		}

		public Query IsLess(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" < [{tableName}].[{tableColumnName}]");
			return this;
		}

		public Query IsLike(string template) {
			_sqlText.AppendLine($" LIKE '{template}'");
			return this;
		}

		public Query IsBetween(object val1, object val2) {
			_sqlText.AppendLine($" BETWEEN {ValueToStr(val1)} AND {ValueToStr(val2)} ");
			return this;
		}

		public Query IsBetween(string column1, string column2) {
			_sqlText.AppendLine($" BETWEEN [{column1}] AND [{column2}] ");
			return this;
		}

		public Query IsBetween(string table1, string column1, string table2, string column2) {
			_sqlText.AppendLine($" BETWEEN [{table1}].[{column1}] AND [{table2}].[{column2}] ");
			return this;
		}

		public Query IsBetween(Cast cast1, Cast cast2) {
			_sqlText.Append($" BETWEEN ");
			if (!String.IsNullOrEmpty(cast1.ColumnName)) {
				if (!String.IsNullOrEmpty(cast1.TableName)) {
					_sqlText.Append($" CAST([{cast1.TableName}].[{cast1.ColumnName}]" +
						$" AS {Cast.GetStringType(cast1.Type)})");
				} else {
					_sqlText.Append($" CAST([{cast1.ColumnName}]" +
						$" AS {Cast.GetStringType(cast1.Type)})");
				}
			} else if (cast1.Value != null) {
				_sqlText.Append($" CAST({ValueToStr(cast1.Value)} AS {Cast.GetStringType(cast1.Type)})");
			}
			_sqlText.Append($" AND ");
			if (!String.IsNullOrEmpty(cast2.ColumnName)) {
				if (!String.IsNullOrEmpty(cast2.TableName)) {
					_sqlText.Append($" CAST([{cast2.TableName}].[{cast2.ColumnName}]" +
						$" AS {Cast.GetStringType(cast2.Type)})");
				} else {
					_sqlText.Append($" CAST([{cast2.ColumnName}]" +
						$" AS {Cast.GetStringType(cast2.Type)})");
				}
			} else if (cast2.Value != null) {
				_sqlText.Append($" CAST({ValueToStr(cast2.Value)} AS {Cast.GetStringType(cast2.Type)})");
			}
			return this;
		}

		public virtual Query Where() {
			_sqlText.AppendLine(" WHERE ");
			return this;
		}

		public virtual Query Where(string tableColumnName) {
			_sqlText.AppendLine($" WHERE [{tableColumnName}] ");
			return this;
		}

		public virtual Query Where(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" WHERE [{tableName}].[{tableColumnName}] ");
			return this;
		}

		public Query OpenBlock() {
			_sqlText.AppendLine(" ( ");
			return this;
		}

		public Query CloseBlock() {
			_sqlText.AppendLine(" ) ");
			return this;
		}

		public Query And() {
			_sqlText.AppendLine(" AND ");
			return this;
		}

		public Query Or() {
			_sqlText.AppendLine(" OR ");
			return this;
		}

		public Query And(string tableColumnName) {
			_sqlText.AppendLine($" AND [{tableColumnName}] ");
			return this;
		}

		public Query Or(string tableColumnName) {
			_sqlText.AppendLine($" OR [{tableColumnName}] ");
			return this;
		}

		public Query And(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" AND [{tableName}].[{tableColumnName}] ");
			return this;
		}

		public virtual Query Where(Upper upper) {
			_sqlText.Append(" WHERE UPPER(");
			if (!String.IsNullOrEmpty(upper.TableName)) {
				_sqlText.Append($"[{upper.TableName}].[{upper.ColumnName}]");
			} else {
				_sqlText.Append($"[{upper.ColumnName}]");
			}
			_sqlText.AppendLine(")");
			return this;
		}

		public virtual Query Where(Lower lower) {
			_sqlText.Append(" WHERE LOWER(");
			if (!String.IsNullOrEmpty(lower.TableName)) {
				_sqlText.Append($"[{lower.TableName}].[{lower.ColumnName}]");
			} else {
				_sqlText.Append($"[{lower.ColumnName}]");
			}
			_sqlText.AppendLine(")");
			return this;
		}

		public Query Where(Cast cast) {
			if (!String.IsNullOrEmpty(cast.ColumnName)) {
				if (!String.IsNullOrEmpty(cast.TableName)) {
					_sqlText.AppendLine($" WHERE CAST([{cast.TableName}].[{cast.ColumnName}]" +
						$" AS {Cast.GetStringType(cast.Type)}) ");
				} else {
					_sqlText.AppendLine($" WHERE CAST([{cast.ColumnName}]" +
						$" AS {Cast.GetStringType(cast.Type)}) ");
				}
			} else if (cast.Value != null) {
				_sqlText.Append($" WHERE CAST({ValueToStr(cast.Value)} AS {Cast.GetStringType(cast.Type)})");
			}
			return this;
		}

		public virtual Query And(Upper upper) {
			_sqlText.Append(" AND UPPER(");
			if (!String.IsNullOrEmpty(upper.TableName)) {
				_sqlText.Append($"[{upper.TableName}].[{upper.ColumnName}]");
			} else {
				_sqlText.Append($"[{upper.ColumnName}]");
			}
			_sqlText.AppendLine(")");
			return this;
		}

		public virtual Query And(Lower lower) {
			_sqlText.Append(" AND LOWER(");
			if (!String.IsNullOrEmpty(lower.TableName)) {
				_sqlText.Append($"[{lower.TableName}].[{lower.ColumnName}]");
			} else {
				_sqlText.Append($"[{lower.ColumnName}]");
			}
			_sqlText.AppendLine(")");
			return this;
		}

		public Query And(Cast cast) {
			if (!String.IsNullOrEmpty(cast.ColumnName)) {
				if (!String.IsNullOrEmpty(cast.TableName)) {
					_sqlText.AppendLine($" AND CAST([{cast.TableName}].[{cast.ColumnName}]" +
						$" AS {Cast.GetStringType(cast.Type)}) ");
				} else {
					_sqlText.AppendLine($" AND CAST([{cast.ColumnName}]" +
						$" AS {Cast.GetStringType(cast.Type)}) ");
				}
			} else if (cast.Value != null) {
				_sqlText.Append($" AND CAST({ValueToStr(cast.Value)} AS {Cast.GetStringType(cast.Type)})");
			}
			return this;
		}

		public virtual Query Or(Upper upper) {
			_sqlText.Append(" OR UPPER(");
			if (!String.IsNullOrEmpty(upper.TableName)) {
				_sqlText.Append($"[{upper.TableName}].[{upper.ColumnName}]");
			} else {
				_sqlText.Append($"[{upper.ColumnName}]");
			}
			_sqlText.AppendLine(")");
			return this;
		}

		public virtual Query Or(Lower lower) {
			_sqlText.Append(" OR LOWER(");
			if (!String.IsNullOrEmpty(lower.TableName)) {
				_sqlText.Append($"[{lower.TableName}].[{lower.ColumnName}]");
			} else {
				_sqlText.Append($"[{lower.ColumnName}]");
			}
			_sqlText.AppendLine(")");
			return this;
		}

		public Query Or(Cast cast) {
			if (!String.IsNullOrEmpty(cast.ColumnName)) {
				if (!String.IsNullOrEmpty(cast.TableName)) {
					_sqlText.AppendLine($" OR CAST([{cast.TableName}].[{cast.ColumnName}]" +
						$" AS {Cast.GetStringType(cast.Type)}) ");
				} else {
					_sqlText.AppendLine($" OR CAST([{cast.ColumnName}]" +
						$" AS {Cast.GetStringType(cast.Type)}) ");
				}
			} else if (cast.Value != null) {
				_sqlText.Append($" OR CAST({ValueToStr(cast.Value)} AS {Cast.GetStringType(cast.Type)})");
			}
			return this;
		}

		public Query Or(string tableName, string tableColumnName) {
			_sqlText.AppendLine($" OR [{tableName}].[{tableColumnName}] ");
			return this;
		}

		public Query Not() {
			_sqlText.AppendLine(" NOT ");
			return this;
		}

		public Query OrderByRandom() {
			_sqlText.AppendLine($" ORDER BY NEWID()");
			return this;
		}

		public Query OrderByAsc(string columnName) {
			_sqlText.AppendLine($" ORDER BY [{columnName}] ASC");
			return this;
		}

		public Query OrderByAsc(string tableName, string columnName) {
			_sqlText.AppendLine($" ORDER BY [{tableName}].[{columnName}] ASC");
			return this;
		}

		public Query OrderByDesc(string columnName) {
			_sqlText.AppendLine($" ORDER BY [{columnName}] DESC");
			return this;
		}

		public Query OrderByDesc(string tableName, string columnName) {
			_sqlText.AppendLine($" ORDER BY [{tableName}].[{columnName}] DESC");
			return this;
		}

		public Query ThenByRandom() {
			_sqlText.AppendLine($", NEWID()");
			return this;
		}

		public Query ThenByAsc(string columnName) {
			_sqlText.AppendLine($", [{columnName}] ASC");
			return this;
		}

		public Query ThenByAsc(string tableName, string columnName) {
			_sqlText.AppendLine($", [{tableName}].[{columnName}] ASC");
			return this;
		}

		public Query ThenByDesc(string columnName) {
			_sqlText.AppendLine($", [{columnName}] DESC");
			return this;
		}

		public Query ThenByDesc(string tableName, string columnName) {
			_sqlText.AppendLine($", [{tableName}].[{columnName}] DESC");
			return this;
		}

		public Query Exists(Select select) {
			_sqlText.AppendLine($" EXISTS ({select.SqlText}) ");
			return this;
		}
	}
}
